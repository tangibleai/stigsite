from django.apps import AppConfig


class DiscordloginConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "discordlogin"

    def ready(self):
        """ Necessary to import here to ensure the post_save signal is picked up """
        import discordlogin.signals
