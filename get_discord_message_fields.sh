#!/usr/bin/env bash
python manage.py shell_plus << EOF
from stigapp.models import DiscordMessage
from stigapp.models import TopMessage
from stigapp.models import GardenMessage
discord_message_fields = [f.name for f in DiscordMessage._meta.get_fields()]
top_message_fields = [f.name for f in TopMessage._meta.get_fields()]
garden_message_fields = [f.name for f in GardenMessage._meta.get_fields()]
fields_dict = {
    'discord_message_fields': discord_message_fields,
    'top_message_fields': top_message_fields,
    'garden_message_fields': garden_message_fields
}
fields_dict
EOF
