# Deployment

## Make accounts

Create an account at each of the following sites:

1. [Render](https://render.com/)
2. [Discord](https://discord.com/)
3. [Supabase](https://supabase.com/)
4. [Airtable](https://airtable.com/)

## Set up a local environment

1. Clone the repository

```bash
 git clone https://gitlab.com/tangibleai/stigsite
```

2. Download the project dependencies in a virtual environment

```bash
python3 -m venv env
source env/bin/activate
pip install -r requirements.txt
```
Note these instructions assume you are using Python 3.9.

## Create an .env file

The .env file will contains the credentials necessary to use the Discord bot like the Discord authentication service, Discord webhook, and Supabase database.  It will also contain Django settings and an Airtable integration experiment.

Throughout the following sections, update the `.env` file below with the relevant values as you create the stigsite components:

```bash
# Supabase database credentials ########

## Used in settings.py
SUPABASE_DOMAIN=
SUPABASE_DB_HOST=db.${SUPABASE_DOMAIN}
SUPABASE_PW=
## Used in log_discord_message.py and loggers.py
SUPABASE_URL=https://${SUPABASE_DOMAIN}
SUPABASE_KEY=

# Discord bot credentials ##############
## Used in log_discord_message.py
DISCORD_BOT_ID=
DISCORD_BOT_TOKEN=

# Discord user authentication ##########
## Used in discordlogin/views.py
DISCORD_LOGIN_AUTH_URL=
DISCORD_LOGIN_CLIENT_ID=
DISCORD_LOGIN_CLIENT_SECRET=

# Airtable credentials #################
## Used in stigapp/airtable_integration_supabase.py
## (only a test currently)
AIRTABLE_API_KEY=
AIRTABLE_BASE_ID=
AIRTABLE_TABLE_NAME_TOP_MESSAGES=
AIRTABLE_TABLE_NAME_GARDEN=

# Django Settings ######################
## Used in settings.py
DEBUG=False
TIME_ZONE=  # Ex. US/Pacific
```

## Set up Supabase database

1. Sign in to [Supabase ](https://supabase.com/).

2. From the home page, click **New project**.

3. Click on **New organization**.

4. Enter in an organization name, such as 'TEC'.  Click **Create organization**.

5. In the additional fields that appear, add a Project name, such as "stigsite.""

6. Add a strong database password.

7. Enter this password in the `.env` file under "Supabase database credentials" **SUPABASE_PW**.

8. When the project configuration data will display. Click **Copy** in the Project API keys box.

9. Add this key to the `.env` file under "Supabase database credentials" **SUPABASE_KEY**.

10. Go to the *project settings* and click on *database* at the right sidebar.

11. Go to the *connection info*, and copypaste the Host without **db.** (eg., zcdrteggwbuifufjguagwv.supabase.co).

12. Paste the Host under "Supabase database credentials" **SUPABASE_DOMAIN**.

## Create the database for Django

Make migrations and migrate to create some Django default tables for Supabase:

```python
python manage.py makemigrations
python manage.py migrate
```
The following tables are the default tables that Django will create:

1. **auth_group**
2. **auth_group_permission**
3. **auth_permission**
4. **django_admin_log**
5. **django_content_type**
6. **django_migrations**
7. **django_session**
8. **django_site**

Django will create the tables above during set up (migration). So, you will need to verify that these tables are present on Supabase for the application to work before you move to the next step.

## Set up Supabase tables

You need to manually make the tables below for Stigapp to work. You can add the tables below by going to the **Table editor** section of your Supabase organization's project, and click on *New table*:

|Table Name | Description |
|---        |---          |
| auth_user | Users who have logged in with their Discord account |
| auth_user_groups | A dummy table to satisfy Django's delete function |
| auth_user_user_permissions | A dummy table to satisfy Django's delete function |
| community_profile | Settings for a particular community's version of Stigsite |
| discord_message | Messages from a Discord server collected by the log_message_data Discord Bot |
| discord_reaction | Reacts from a Discord server collection by the log_message_data Discord Bot |
| extracted_urls | URLs a user included in their Discord message |
| garden_message | A collection of messages chosen by Gardener's from the **top_message** table |
| log | A record of events in log_message_data Discord Bot |
| reward_reaction_log | Handles management of the reward emoji when a post is added from the top_message table to the garden_message table |
| stigapp_rule | Conditions that determine when and what Stigapp does with particular messages |
| top_message | A collection of messages from **discord_message** that have met threshold conditions |

### 1) auth_user

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | flags | int8 | NULL |
   | public_flags | int8 | NULL |
   | avatar | varchar | NULL |
   | discord_tag | varchar | NULL |
   | email | varchar | NULL |
   | locale | varchar | NULL |
   | first_name | varchar | NULL |
   | last_name | varchar | NULL |
   | password | varchar | NULL |
   | twitter_handle | varchar | NULL |
   | twitter_username | varchar | NULL |
   | username | varchar | NULL |
   | date_joined | timestamptz | NULL |
   | last_login | timestamptz | NULL |
   | active | bool | NULL |
   | admin | bool | NULL |
   | mfa_enabled | bool | NULL |
   | staff | bool | NULL |
   | requested_twitter_handle | bool | NULL |
   | community_profile_id | foreign key (int4) | Foreign key to the community_profile table id field |
   | django_site_id | foreign key (int4) | Foreign key to the django_site table id field |

### 2) auth_user_groups

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | group_id | int8 | NULL |
   | user_id | int8 | NULL |

### 3) auth_user_user_permissions

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | permission_id | int8 | NULL |
   | user_id | int8 | NULL |

### 4) community_profile

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | community_members | jsonb | NULL |
   | community_name | varchar | NULL |
   | monitored_channels | jsonb | NULL |
   | monitored_guilds | jsonb | NULL |
   | promotion_threshold | int8 | NULL |
   | site_style | jsonb | NULL |
   | django_site_id | foreign key (int4) | Foreign key to the django_site table id field |
   | airtable_credentials | jsonb | NULL |
   | enable_twitter_handle_request | boolean | false |

### 5) discord_message

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | author | jsonb | NULL |
   | forum_tags | jsonb | NULL |
   | reactions | jsonb | NULL |
   | authorship_datetime | timestamptz | NULL |
   | created_at | timestamptz | now() |
   | edited_at | timestamptz | NULL |
   | saved_at | timestamptz| now() |
   | author_discord_username | varchar | NULL |
   | content | varchar | NULL |
   | channel_name | varchar | NULL |
   | guild_name | varchar | NULL |
   | message_thread_title | varchar | NULL |
   | twitter_handle | varchar | NULL |
   | jump_url | varchar | NULL |
   | message_type | varchar | NULL |
   | django_site_id | foreign key (int4) | Foreign key to the django_site table id field |
   | extracted_urls_id | foreign key (int8) | Foreign key to the extracted_urls table id field |

### 6) discord_reaction

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | discord_message | int8 | NULL |
   | message_id | int8 | NULL |
   | user | int8 | NULL |
   | created_at | timestamptz | now() |
   | emoji | text | NULL |
   | previous_reactions | jsonb | NULL |
   | previous_reaction_users | jsonb | NULL |
   | users | jsonb | NULL |

### 7) extracted_urls

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | url_list | jsonb | NULL |

### 8) garden_message

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | message_id | int8 | NULL |
   | unique_reaction_count | int8 | NULL |
   | user_count | int8 | NULL |
   | unique_user_count | int8 | NULL |
   | original_message | text | NULL |
   | editable_message | text | NULL |
   | author | jsonb | NULL |
   | counts_by_emoji | jsonb  | NULL |
   | emoji_list | jsonb | NULL |
   | forum_tags | jsonb | NULL |
   | reactions | jsonb | NULL |
   | unique_emoji_list | jsonb | NULL |
   | unique_user_list | jsonb | NULL |
   | user_list | jsonb | NULL |
   | reaction_count | float4 | NULL |
   | authorship_datetime | timestamptz | NULL |
   | created_at | timestamptz | now() |
   | edited_at | timestamptz | NULL |
   | exported_at | timestamptz | NULL |
   | author_discord_username | varchar | NULL |
   | channel_name | varchar | NULL |
   | guild_name | varchar | NULL |
   | user_id | varchar | NULL |
   | user_list | varchar | NULL |
   | twitter_handle | varchar | NULL |
   | jump_url | varchar | NULL |
   | message_thread_title | varchar | NULL |
   | message_type | varchar | NULL |
   | django_site_id | foreign key (int4) | Foreign key to the django_site table id field |
   | extracted_urls_id | foreign key (int8) | Foreign key to the extracted_urls table id field |

### 9) log

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | created_at | timestamptz | now() |
   | json | jsonb | NULL |
   | text | varchar | NULL |
   | log_level | int2 | NULL |

### 10) reward_reaction_log

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | created_at | timestamptz | now() |
   | message_id | int8 | NULL |
   | reaction_sent | boolean | NULL |

### 11) stigapp_rule

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | channel_name | varchar | NULL |
   | role | varchar | NULL |
   | rule_name | varchar | NULL |
   | activated_status | boolean | NULL |
   | chosen_reaction | jsonb | NULL |
   | threshold | float8 | NULL |

### 12) top_message

   | field | type | default |
   |---    |---   |---      |
   | id | int8 | PRIMARY |
   | message_id | int8 | NULL |
   | unique_reaction_count | int8 | NULL |
   | unique_user_count | int8 | NULL |
   | user_count | int8 | NULL |
   | reaction_count | float4 | NULL |
   | original_message | text | NULL |
   | editable_message | text | NULL |
   | added_to_garden | bool | false |
   | author | jsonb | NULL |
   | counts_by_emoji | jsonb | NULL |
   | emoji_list | jsonb | NULL |
   | forum_tags | jsonb | NULL |
   | reactions | jsonb | NULL |
   | unique_emoji_list | jsonb | NULL |
   | unique_user_list | jsonb | NULL |
   | user_list | jsonb | NULL |
   | authorship_datetime | timestamptz | NULL |
   | created_at | timestamptz | now() |
   | edited_at | timestamptz | NULL |
   | exported_at | timestamptz | NULL |
   | author_discord_username | varchar | NULL |
   | channel_name | varchar | NULL |
   | guild_name | varchar | NULL |
   | message_thread_title | varchar | NULL |
   | twitter_handle | varchar | NULL |
   | jump_url | varchar | NULL |
   | message_type | varchar | NULL |
   | django_site_id | foreign key (int4) | Foreign key to the django_site table id field |
   | extracted_urls_id | foreign key (int8) | Foreign key to the extracted_urls table id field

* **NOTE:** Stigapp rewards posts with an emoji reaction when they move from the top_message table to the garden_message table. Stigsite and the Discord bot use the database table `reward_reaction_log` to communicate about the reward.  Stigsite will add an entry for a post that should receive the reaction.  Each time a reaction is added, the Discord bot checks any eligible messages in `reward_reaction_log`, issues the emoji, and changes the `reaction_sent` column to True.

## Get a Supabase SSL certificate

You will need to include the contents of this certificate into the Render web hosting service later:

1. Click on **Settings** (the gear icon).

2. Next, click on **Database**.

3. Then, scroll down to the "SSL Connection" section.

4. Click on **Download Certificate**.

## Set up the Discord components

### Create a Discord bot

1. Log in to the [Discord Developer Portal](https://discord.com/developers/applications) with your Discord login credentials.

2. Click on *New Application*.

3. Give the chatbot a name, and click on *Create* after you agree to the Terms of Service.

4. Copy the APPLICATION ID. Then, put it in the `.env` file under "Discord bot credentials" **DISCORD_BOT_ID**.

5. From the sidebar, click on **Bot**.

6. Click **Add Bot**. Then hit **Yes, do it!**.

7. Next, click on **Reset Token**.

8. Hit **Yes, do it!**.

9. Then, click **Copy**.

10. You should put this token in the `.env` file under "Discord bot credentials" **DISCORD_BOT_TOKEN**.

11. From the sidebar, click **OAuth2**.

12. Select **URL Generator**.

13. In SCOPES, select **bot**.

14. In BOT PERMISSIONS, select all the checkboxes under TEXT PERMISSIONS.

15. In GENERATED URL, click **Copy**.

16. Visit the URL in your browser.

17. Select the server you want the bot to work in.

18. Click on **Continue**.  Then, hit the **Authorize** button.

### Create a Discord identity application

1. Log in to the [Discord Developer Portal](https://discord.com/developers/applications) with your Discord login.

2. Click on *New Application*.

3. Give the identity application a name, such as StigIdentity. Then, click on *Create* after you agree to the Terms of Service.

4. From the sidebar, click **OAuth2**.

5. Click **Reset Secret**. Then, hit **Yes, do it**.

6. Click **Copy**.

7. Put the CLIENT SECRET in the `.env` file under "Discord user authentication" **DISCORD_LOGIN_CLIENT_SECRET**.

8. Under "Client ID", click **Copy**.

9. Save what you copied in the `.env` file under "Discord user authentication" **DISCORD_LOGIN_CLIENT_ID**.

10. Click on **Add Redirect** under "Redirects", .

11. Enter the redirect for the server you are working on. Note the endpoint should be "/oauth2/login/redirect" (ie., http://localhost:8000/oauth2/login/redirect).

12. Click **Save Changes** when finished.

13. Select **URL Generator**.

14. In SCOPES, select **identify**.

15. In SELECT REDIRECT URL, select a redirect URL from the dropdown menu (Example: http://localhost:8000/oauth2/login/redirect).

16. Under GENERATED URL, click **Copy**.

17. Save the generated url in the `.env` as **DISCORD_LOGIN_AUTH_URL**.

## Set up Airtable integration

To connect to your Airtable account, Stigsite needs 4 pieces of information:

* **AIRTABLE_API_KEY**
* **AIRTABLE_BASE_ID**
* **AIRTABLE_TABLE_NAME_TOP_MESSAGES**
* **AIRTABLE_TABLE_NAME_GARDEN**

Here's what you need to do to retrieve this information.

1. Log in to [Airtable](https://airtable.com/).

2. Click on the profile image.

3. Click **Account**.

4. Under the API section, click **Generate API key**.

5. Copy the API key and paste it next to **AIRTABLE_API_KEY** in the `.env` file.

7. Go back to your Airtable home page, and create an Airtable base.

8. The base will have an Airtable URL like this example:
https://airtable.com/appbDrod2abrajcNS/tblsdJYFSJclFAHAA/viwDjfshEw4gHDhDUq?blocks=hide

9. Copy the portion of the URL that starts with "app" (ie., "appbDrod2abrajcNS").  Add this to the **AIRTABLE_BASE_ID** in the `.env` file.

10. Create a `TopMessage`s table within that base (you can name it whatever you like).

11. Copy the table name in the tab above the table you created.

12. Add this to the **AIRTABLE_TABLE_NAME_TOP_MESSAGES** variable in the `.env` file.

13. Repeat steps 10 and 11 for the `GardenMessage`s table and add your new table name to the variable **AIRTABLE_TABLE_NAME_GARDEN** in the `.env` file.

Note the **GardenMessage** and **TopMessage** tables have some similar fields, you can duplicate the first table and then modify what is needed.

### Airtable Fields

The tables in the base need to include all the fields below.  Capitalization and spacing of each field name matters.

#### TopMessage Table

   | Field Name | Type | Additional Settings |
   |---         |---   |---                  |
   | Author | Single line text | NA |
   | Twitter Handle | Single line text | NA |
   | Guild | Single line text | NA |
   | Channel | Single line text | NA |
   | Message ID | Single line text | NA |
   | Message Type | Single line text | NA |
   | Thread Title | Single line text | NA |
   | Original Message | Long text | NA |
   | Authorship | Date | ISO, Include time, 24 hour, Display time zone |
   | Editable Message | Long text | NA |
   | Edited At | Date | ISO, Include time, 24 hour, Display time zone |
   | All Reactions | Number |  Integer |
   | Unique Reactions | Number | Integer |
   | All Emojis | Single line text | NA |
   | Unique Emojis | Single line text | NA |
   | Totals by Emoji | Single line text | NA |
   | Total Users | Number |  Integer |
   | Unique Users | Number |  Integer |
   | Total User List | Single line text | NA |
   | Unique User List | Single line text | NA |
   | Exported At | Date | ISO, Include time, 24 hour, Display time zone |
   | Extracted URL | URL | NA |
   | Jump URL | URL | NA |
   | Added to Garden | Checkbox | NA |

#### GardenMessage Table

   | Field Name | Type | Additional Settings |
   |---         |---   |---                  |
   | Author | Single line text | NA |
   | Twitter Handle | Single line text | NA |
   | Guild | Single line text | NA |
   | Channel | Single line text | NA |
   | Message ID | Single line text | NA |
   | Message Type | Single line text | NA |
   | Thread Title | Single line text | NA |
   | Original Message | Long text | NA |
   | Authorship | Date | ISO, Include time, 24 hour, Display time zone |
   | Editable Message | Long text | NA |
   | Edited At | Date | ISO, Include time, 24 hour, Display time zone |
   | All Reactions | Number |  Integer |
   | Unique Reactions | Number | Integer |
   | All Emojis | Single line text | NA |
   | Unique Emojis | Single line text | NA |
   | Totals by Emoji | Single line text | NA |
   | Total Users | Number |  Integer |
   | Unique Users | Number |  Integer |
   | Total User List | Single line text | NA |
   | Unique User List | Single line text | NA |
   | Exported At | Date | ISO, Include time, 24 hour, Display time zone |
   | Extracted URL | URL | NA |
   | Jump URL | URL | NA |

## Start the server

1. Run your Django server and start playing around with `stigsite`:

```python
python manage.py runserver
```
Note this is only suitable for the local version. Production versions on a server will use a command from gunicorn, uvicorn, or daphne.

## Set up Render web service

In order to follow these steps, you will need to enter credit card information for the account because we will be using the Blueprint feature.

Using the Blueprint feature, you will be able to spin up new instances of the application with similar settings based on a common Git*** repository.

However, you will still be able to define the features unique to each project, such as the `.env`credentials from above.

1. Log in to [Render](https://render.com/).

2. Click **Blueprints**.

3. Click **New Blueprint Instance**.

4. Enter into the **Public Git repository** field the repository you will be using (ie.,  https://gitlab.com/tangibleai/stigsite).  Then, click **Continue**.

* **NOTE:** The current application requires hardcoding values for things like approved admin users.  You will need to adjust the `constants.py` file or the `discordlogin/models.py` users to your project's needs.

5. For the Service Group Name, enter a project name, such as stigsite.

6. Click **Create New Resources**. Then, hit **Apply**.

7. Render will start to build the application.  However, the application will fail because we need to set up the `.env` files and build/start commands.  For this reason, let's click on **Dashboard**.

8. Then, click on the **New application** to create a new application.

9. Click on **Environment**.

10. Click **Add Secret File**.

11. For Filename, write **.env**.  For File contents, copy the contents of the `.env` file you have built throughout this document.

12. Click on **Add Secret File** again.

13. For Filename, write **secrets_prod-ca-2021.crt** (or whatever the name of that file that you downloaded is). Then, copy the contents of that file.

14. Click **Save Changes**.

15. Click **Events**.

16. Click on **Deploy** in the most current deployment.  This is evident by three dots undulating.

17. When the application successfully deploys, Render will display the word "live" in a green box.  The terminal will display the following:

```
Sep 20 05:16:04 PM  ==> Uploading build...
Sep 20 05:16:31 PM  ==> Build uploaded in 12s
Sep 20 05:16:31 PM  ==> Build successful 🎉
Sep 20 05:16:31 PM  ==> Deploying...
Sep 20 05:17:33 PM  ==> Starting service with 'source ./start.sh'
Sep 20 05:17:33 PM  starting pycord service: python log_discord_messages.py &
Sep 20 05:17:33 PM  gunicorn --threads=2 stigsite.wsgi:application
Sep 20 05:17:38 PM  2022-09-20 08:17:38,019:WARNING - PyNaCl is not installed, voice will NOT be supported
Sep 20 05:17:38 PM  2022-09-20 08:17:38,021:INFO - logging in using static token
Sep 20 05:17:38 PM  2022-09-20 08:17:38,525:INFO - Shard ID None has sent the IDENTIFY payload.
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [55] [INFO] Starting gunicorn 20.1.0
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [55] [INFO] Listening at: http://0.0.0.0:10000 (55)
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [55] [INFO] Using worker: gthread
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [58] [INFO] Booting worker with pid: 58
Sep 20 05:17:38 PM  [2022-09-20 08:17:38 +0000] [60] [INFO] Booting worker with pid: 60
```
Enjoy it!
