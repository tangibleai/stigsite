# Sprint Plan

## Sprint 05: Sept 6 - Sept 12

- [X] G6-2: Log/update message script (create_leaderboard_data) - integrate into the Django application (Python manage.py shell) (cat <script_name> | python mange.py shell )
  * Integrate create_leaderboard_data into Django application (cat <script_name> | python manage.py shell) (Should cause signal to fire)
  * Signal: when added_to_garden is True, get_or_create GardenMessage
  * Signal: if get_or_create is already present, update relevant GardenMessage
  * Add "added_to_garden" to TopMessage and replicate functionality of LeaderBoard
  * Add GardenMessage to Supabase, and update models.py
  * Update GardenMessage model to include an id and all of the LeaderBoard fields
  * Remove LeaderBoard table and other unused kruft
- [X] G8-3: Make DiscordLogin work with admin interface
  * Research Python inheritance
  * Make DiscordUser inherit admin model (django.models.auth.user / django.contrib.admin)
  * User can log in with Discord and use admin interface
  * Make sure regular authentication works
  * Restrict the admin permisions and make it so they can be added later
  * Toggle visibility of "Admin" link in navbar based on is_admin/is_staff or not
  * Superuser can use admin interface to designate Discord users as superusers
  * Make a fake password for the Django side authentication (it doesn't work even if someone tries to log in with it, but is necessary because it's a mandatory, nonnullible field)
- [X] G4-2: Export csv button (TopMessages)
  * Trigger a view.py function to export a csv from the profile template
  * Customize the export to use the TopMessage table
- [X] G6-8.5: Send a Reward emoji (flower) to a message id for getting sent to garden
  * Write a function that adds an emoji to a post based on message_id\
  * Set up a private admin channel
  * Create a webhook for the server that sends messages to the admin channel
  * Make a signal function run when LeaderBoard/TopMessage added_to_garden is set to yes (should be able to handle multiple rows getting updated at once)
  * Make the signal share to the admin channel the message id of the message when added_to_garden is set to yes
  * Write a function in log_discord_message that listens for activity in the admin channel
  * Call the add_reaction_to_top_message function
  * Make add_reaction_to_top_message search multiple channels for a message
- [X] HG?-1: Channels-Pycord (Channels-discord) integration installed in Stigsite
  * Read through the documentation
  * Tried to integrate it into Django (ModuleNotFoundError: No module named 'stigsite'
- [X] G1-1: Ensure the author field persists from DiscordMessage > TopMessage > GardenMessage
  * Message author recorded in TopMessage table
  * Ensure GardenMessage also records the author from TopMessage
  * Add message_id to TopMessage and GardenMessage for better filtering
- [X] G1-1: Garden table inherits TopMessage model (inheritance can't happen between databases) (Make sure the database is supabase for the models)
- [X] HG?-2: Review past two videos of Sprint planning with Ronin and Shahar (on Discord - Tuesday meetings)


## Backlog

- [ ] M: Create a diagram of the 4 users and their stories
- [ ] H: Fix  None, None, None users in log_discord_message.py and Supabase DiscordMessage(?)
- [ ] H: Clean up old branches
- [ ] H: merge to main and/or delete feature-modularize and feature-django-orm 

- [ ] G: Receive signal - check for change to added_to_garden - if yes, then make GardenMessage

## Don't do

- [ ] G: Garden table inherits TopMessage model (inheritance can't happen between database) (Make sure the database is supabase for that model)
- [ ] H: Review Shahar's rules on Notion
- [ ] HG: Review past two videos of Sprint planning with Ronin and Shahar (on Discord - Tuesday meetings)
  * Contact for access to videos (Access received!)
- [ ] H: Clean up old branches
- [ ] G: General bug fixes
