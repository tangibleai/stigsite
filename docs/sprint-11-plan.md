## Sprint-11-plan

Wed Oct 19 - Oct 26

### DAOStack requests for TEC

- [x] G2-2 URGENT: ASAP merge to main and deploy to stigsite-tec.onrender.com with airtable env etc
  * BUG: Add a rule in the rule table because there isn't one now.

- [x] G1-2.5 Make Airtable env variables
  * [x] Add env variables for AIRTABLE KEY, BASE, TOPTABLE, GARDENTABLE strings
  * [x] Test Airtable env for DAOStack dogfood config info from Ronen in the BitWarden collection named "DAOStack Stigsite" (".env Stigdata Stigsite")
  * [x] Test Airtable env for TEC main site

- [x] G1.5-1.5: editable_message field in `TopMessage` table
  * [x] only sync with Airtable once when first creating it from a copy of the original_message content
    - Note: I've built in the capability to sync the TopMessage Airtable with the Supabase TopMessage table on every reaction, but I've commented it out for now.  It can easily be re-implemented later.
  * [x] GardenMessage table should inherit the value when first created from a TopMessage

- [x] G3-3.5: 3 additional fields in all 5 tables: **DiscordMessage**, TopMessage, GardenMessage and both airtable syncs
  * [x] Fields
    - [x] 1. discord message authorship datetime field
    - [x] 2. discord channel name field (where the message was first posted) in the TopMessages and GardenMessage table
    - [x] 3. discord guild/server name field
  * [x] Add all additional fields to database tables
    - Fields
      * DM/TM/GM: message_authorship_date
      * DM/TM/GM: channel_name
      * DM/TM/GM: guild_name
    - [x] 0.5-0.7- DiscordMessage
    - [x] TopMessage
    - [x] GardenMessage
  * [x] Add all additional fields to all Airtables
    - Fields
      * TM/GM: message_authorship_date
      * TM/GM: channel_name
      * TM/GM: guild_name
      * TM: editable_message field
    - [x] Local/testing Airtable
    - [x] Staging/CSm Airtable
    - [ ] Production/TEC Airtable

- Bug fixes (Times included in above tasks)
  * [x] Fix issue where TopMessage Airtable added_to_garden field value is not updated
  * [x] Fix issue where message deleted from TopMessage table is not deleted from TopMessage Airtable (GardenMessage is still not deleted)
  * [x] Fix issue where entries in TopMessage and TopMessage Airtable are not created when DiscordMessage exists in on_raw_reaction_remove
  * [x] Add backwards compatibility for old fields that don't have data for new values added this sprint
  * [x] Automatically add a GardenMessage to the GardenMessage Airtable when it is created through the Stigsite admin interface

- General Cleanup
  * [x] G2-1.5: Consolidate models.py files and classes
  * [x] G0.5-0.2: Remove Django-cron
  * [x] G0.5-1: Make GardenMessage Airtable sync button work with airtable_integration_supabase.py
  * [x] G0.5-0.1: Remove airtable_integration.py

- [x] Update reaction threshold
  * [x] G2-1: reaction threshold should use >= instead of just > and should be able to handle and threshold value of 1.
  * [x] G1-1: 0 reaction messages recorded in discord_message and rule propogates to all other tables
  * [x] G0.5-0.2: help text for threshold field in Rule models.py should describe the **greater than or equal to** comparision operation of the rule (not grater than).

- [ ] run the [maitag/notebooks/machine-assisted-intent-tagging.ipynb]() with active learning by predicting the output or printed representation of the object(s) created in each cell

- [x] G1-1.5: Review `sites.models.Site` [documentation](https://docs.djangoproject.com/en/4.1/ref/contrib/sites/)
- [ ] Create `class CommunityProfile(django.contrib.sites.models.Site)` in `discordlogin.models` comparable to `discordlogin.models.User` model and `discord_login.views.community_profile` view comparable to `discordlogin.views.profile` and limit access to admin users
  - [ ] Data initialization script or migrate script to create 2 Site records: stigsite-dogfood and stigiste-tec
  - [ ] Create  `site`) = ForeignKey(CommunityProfile) field in User(User) table
  - [ ] Create model-based view CRUD form for editing a single record in the Profile table at https://stigsite-dogfood.onrender.com/community/
  - [ ] Add link from User profile view to `community_profile()` view (`...onrender.com/community/` URL) on (`...onrender.com/profile/` template.html - only visible for `internal-user` and `admin` roles for communities associated with that user
  - [ ] Add community `site` = ForeignKey(CommunityProfile) and records `site__domain_name` in all 3 discord messages tables
  - [ ] Sync site__domain_name to both Airtables?

`- [ ] Fields or key-value pairs for JSONField in CommunityProfile model and view
  - [ ] list of guilds/servers StigBot listens too (currently must manually add the bot to the discord server)
  - [ ] list of server-channels pairs StigBot listens too JSONField with list of pairs (lists with 2 strs)
  - [ ] pay attention to (enforce) guild name (server name) filter
  - [ ] pay attention to (enforce) guild-channel name pair filter
  - [ ] Airtable config 4 fields such as AIRTABLE_KEY, BASE_NAME, TABLE_NAME(s)
  - [ ] threshold for promotion to TopMessage
  - [ ] community logo for home page
  - [ ] upload button for logo
  - [ ] theme name
  - [ ] add CSS associated with current "light" name in the THEME_CHOICES filename as the hidden text value for the choice
  - [x] G1-2: Add light/dark mode support
    * [x] get Rochdi to help modify CSS for "light" theme and apply it to all end-user and internal-user and garden-user pages
    * [x] get Rochdi to create "dark" theme and any others he would like
    * [x] add CSS associated with "dark" and other Rochdi CSS theme(s)

## Backlog fun blue-sky feature

- [ ] OPTIONAL: implement any additional rule flexibility that you think is high value for TEC or other imagined future Discord communities

- [ ] inc corporate FAQ features into maitag DRF REST API (see qary for additional NLU examples)
  - add qary as dependency or copy FAQ bot code to stigsite
  - create FAQ table and CRUD admin view with same schema as faq.csv from qary
  - replay to messages that contain qary: or similar with FAQ respondes
  - manually mine discord messages table for FAQs and add to FAQ table
  - create script for automatically mining FAQs from discord_message table

- [ ] rule composition UI similar to Zapier UI

- [ ] wireframes of content curation process

# Sprint-10-plan

Wed Oct 11 - Wed Oct 18

## Sprint 10

- [x] G16-24: BUG: ensure that no messages/reactions are lost during deploy before deploying
  * G8-6: on_raw_reaction_add
  * G8-6.5: on_raw_reaction_remove
  * G2-1: Airtable syncing in on_raw_reaction_(add|remove)
  * G8-10.5: Bug testing and refactoring
  (The time estimates/actuals above include all the completed tasks below without times)
- [x] G0.5-0.5: BUG: 500 server error on redirect after discord login for Hobson but not Greg
- [x] sync the reaction count and reaction list to airtable so that discord reactions continue to accumulate in airtable
  * [x] BUG: Duplicate reactions from different users are not being added as separate entries, just updating the count
  * [x] BUG: Custom reactions are not counted initially, but are counted when another reaction is added later
  * [x] BUG: Message added > set add_to_garden = True > message removed from TopMessage, and re-added to TopMessage autmoatically.  Corresponding GardenMessage enty is not updated because 'added_to_garden' has been reset to False.
- [x] live sync of TopMessages/GardenMessages -> Airtable without export button or cron job or user-configurable schedule
  * [x] TopMessage table
  * [x] GardenMessage table
- [-] Form tags status update - In progress - 40% complete
  * [Pycord 2.3](https://github.com/Pycord-Development/pycord/milestone/4)

### deploy Airtable sync and customizable Rule features

- [x] tag next deployed version (release) with `git tag -a 0.0.1 -m "Airtable real-time sync!"`
- [x] create docs/CHANGELOG.md file with features for the next release you plan to deploy:
    ```
    # CHANGELOG

    ## [0.0.1](https://github.com/certego/django-rest-client/releases/tag/0.0.1)

    Initial release

    - User-defined rule for TopMessage table (threshold and emojies list)
    - TopMessage/GardenMessage airtable live (real-time) sync
    - BUGFIX: all reactions are monitored using on_raw_reaction_add()
    ```
- [x] post release notes (CHANGELOG.md) link on DAOStack-CSm Discord *-dev channel
- [x] post release notes link on TEC Discord "Content curation" channel

### medium priority (desired user stories)
- [x] removing emoji on discord reduced count/emojies on TopMessage immediately
- [x] removing emoji on discord reduced count/emojies on GardenMessage immediately
- [x] removing emoji on discord reduced count/emojies on Airtable(s) immediately
- [x] removing emoji on discord reduced count/emojies on TopMessage and potentially removes the TopMessage from the table (depending on a config rule.
- [x] G1-0.5: stigsite-dogfood.onrender.com or similar memorable URL (branding)
- [x] stigsite-TEC.onrender.com or similar memorable URL (branding)
- [x] G2-4: search/filtering in DiscordMessage, TopMessage, GardenMessage admin views


### LOW PRIORITY: Community preferences
- [ ] config data structure in the database so community can manage behavior of the bot "Preferences/Settings"
- [ ] "Preferences/Settings" view
- [ ] Logo customizable so not TEC but CSm
  * [x] G0.5-0.5: Change TEC logo to CSm logo
- [ ] Profile page to contain user settings like the profile photo is the community logo and the community name for that user is the community name displayed on stigitie



## 2022-10-11 DAOStack Meeting notes

Ronen is submitting SBIR grant. Told him about
Gideon is promoting to other partners and communities. 
Encouraged DAOStack to steer marketing towards developers and contributors rather than users. We don't have scalability yet.

- [ ] losing messages freaked TEC out (I should have anticipated this). Ronen will tell them that we want disrupt their workflow again with future deployements
- [ ] wants regular users to only use airtable (updating reactions on existing TopMessages in Airtable sync might put them over the top)

## Customer feature requests

- [x] G1-0.5: dogfood.onrender.com based on staging CICD pipeline (https://dogfood-3jpy.onrender.com)
- [x] G2-2: BUG: test that top messages are updated from the discordmessages table when 5+ rule is True
  * Message Analytics
    - Total DiscordMessages recorded in October: 71
    - DiscordMessages with reactions: 27
  * All DiscordMessage posts with 5+ unique users have been added.
  * On the TEC server, the following three posts meet the threshold.
    - [OceanDAO Is Going Fully Decentralized and Autonomous](https://discord.com/channels/810180621930070088/1028025012780806174/1028025012780806174)
    - [Typology of Web3 Users](https://discord.com/channels/810180621930070088/1027126108216377364/1027126108216377364)
    - [A short primer on Conditional Tokens](https://discord.com/channels/810180621930070088/1026881944027418685/1026881944027418685)
  * Reason: Updates were pushed to main on October 5th and 7th, which resulted in the bot being restarted.  The bot listens to messages/posts sent after it starts.
  * Response to avoid recurrence:
    - We will communicate and coordinate deployments to main so that everyone understands what data is being tracked.
    - Automatic deployments to main have been disabled for now.
    - A long term solution is to look into ways of preserving the bot's ability to handle messages prior to its deployment/restarting.
- [x] G0.5-0.25: Edit your discord profile to reveal your display name as Greg or Greg Thompson
- [x] G10-2: (REDUNDANT IDEAS BELOW) Working user-configurable rules used to determine TopMessages
  - [x] Admin user can adjust the reaction count threshold that determines when messages are added to the TopMessages table
  - Additional
    - hard-coded rule parameters stored in the Rule database table
    - when one rule activated, others deactivated
    - rules ORed together
    - create data structure in python that can be used to call hard-coded rule functions currently used to filter exports of csvs (see log_discord_messages.DEFAULT_ACTIONS)
    - add table Rule (fields like on_export, condition, threshold_value)
    - add table Action (fields: function name, function_args, function_kwargs) -- see log_discord_messages.DEFAULT_ACTIONS...
with fields for 
- [x] G16-6: SEE REDUNDANT SUBTASKS BELOW: filter messages exported based on export date - export all messages since a user specified or selected datetime
  - [x] G: research authentication of an airtable account and connecting it to their Stigsite account (similar to discord authentication, but not used for authentication, just used for py-airtable api requests on behalf of that user)
    * Research Findings
      - User will have to manually enter their own API key that Airtable generates
      - Process: Get Airtable key > Enter into Stigsite > Stigsite uses that Airtable key to afford permissions in a base (If permission is read-only for that base, the API will automatically attribute that user read-only permissions)
  - [x] G: better approach suggested Oct 6: automatic sync to airtable on a repeating schedule
    * [x] Install django-cron
    * [x] Add python manage.py migrate django-cron to build.sh
    * [x] Get cron task working manually (python manage.py runcrons)
    * [x] Add GardenMessage columns to Airtable
    * [x] Update script to pull from GardenMessage table using Django
    * [x] G2-0.5: oneway sync to airtable with a button -  all garden messages all columns (Changed based on the tasks below to only export messages not exported before)
    * [x] G0.5: exports only contain messages not exported previously
    * [x] Test that new airtable code works with manual cron
    * [-] Get cron task working automatically (on my server) (Give up)
    * [x] Get the cron task working on the server (Set to Weekly, Monday, every week)
    * [x] Set the timezone of the application (Still showing Korean timezone in Airtable)
    * [x] Change single create to batch create (To meet the API requests of 5 per second)
    - [-] SEE ABOVE: G8: "click" that button on a recurrening schedule (celery or cron) - weekly on monday morning 8am pacific - only the messages not previously synced to airtable by truly syncing with the airtable table for id in message ids and if not exists in airtable push it up (don't update reaction count for previously exported messages)
    - [-] G: OPTIONAL: overwrite entire table with new airtable export
    - [?] G: config table with name column and value jsonfield
    - [?] User customization not possible because cron commend is set in render.com
      * [-] G4: allow user to specify that cron schedule with airtable_export_date config name and airtable_export_period (in secs, hours, days, weeks, months)
      * [-] G: user can adjust the day/time of the weekly export
    - [ ] G: Get cron working on main server


- [x] G2-1.5 Test bot in staging
  * [x] Bot sends messages to DiscordMessage
  * [x] Bot records reactions to message in DiscordMessage
  * [x] Bot saves DiscordMessage to TopMessage after it reaches the unique user reaction threshold
  * [x] Threshold can be changed and bot can still send DiscordMessage to TopMessage (Default value if not rule is set is 5 unique user emojis)
  * [x] Manually changing `added_to_garden` to True in TopMessage saves the message in GardenMessage
  * [x] Manually changing `added_to_garden` to True in TopMessage adds entry to `reward_reaction_log`
  * [x] An emoji reaction triggers a lookup of `reward_reaction_log`
  * [x] During lookup of `reward_reaction_log` all entries with False for `reaction_sent` receive a flower emoji and `reaction_sent` is changed to `true`
  * [x] GardenMessage Export
    - [x] Test Export - Exports CSV without timestamps / doesn't affect timestamps
    - [x] Export New - Exports CSV with timestamps / Updates `exported_at` field
    - [x] Export to Airtable - Adds unexported entries to the associated Airtable
    - [x] Manually cron run - Adds unexported entries to the associated Airtable (takes awhile, maybe a 1-2 minutes)
  * [x] TopMessage Export
    - [x] Test Export - Exports CSV without timestamps / doesn't affect timestamps
    - [x] Export New - Exports CSV with timestamps / Updates `exported_at` field
  * [x] Profile
    - [x] Displays profile information of the logged in user
    - [x] Can update and save information in the profile
  * [x] Authentication
    - [x] Can log in with Discord for the first time
    - [x] Can change profile as above
    - [x] Can log in with Discord for the second time (After changing profile)
    - [x] Appropriate permissions are set for the user (admin, not admin)
    - [x] Views
      * [x] Admin - can see full site
      * [x] Regular - can see profile, about, and home (without message tables)
    - [x] Can log out
  * [x] Admin site view
    - [x] Can CRUD Users table
    - [x] Can CRUD Discord messages table
    - [x] Can CRUD Garden messages table
    - [x] Can CRUD Rules table
    - [x] Can CRUD Top messages table
  * [x] Changes based on testing
    - [x] "The number of unique user reactions over which the rule applies. The threshold value must be greater 1 or more." > "The number of unique user reactions over which the rule applies. The threshold value must be 1 or more."
    - [x] Update rule_name copy
    - [x] reward_reactions_log should appear in the admin view

## DONT DO

- [-] G: if direct database signaling not possible implement 1 min polling of database in log_discord_messages for notification of changes to the Garden message table (to react with flower)
- [ ] G: alternative ux allows the user to select all with a boolean field on each Garden Message entry for whether the message has been imported into Airtable

## Backlog (Sprint 10)

- [~] G: **WAIT FOR ANSWER FROM DAOSTACK** on CSm discord server: Rename TopMessages->GreenHouse in views (including admin?)
- [~] G: record discord forum message or discord message tags in all tables that contain messages with one of these approaches:
    - [~] G: messages will be tagged with twitter #hashtag style tags within the message text, log_discord_message.py should parse the message text to extract these hashtags and record them similarly to the way reactions are recorded. `re.findall(r'\b#[-_A-Za-z0-9]{2,32}')`
- [ ] G: URLs to other messages by the author of a discord message in one of our 3 tables:
    - [ ] G: implement django-filters or full text search on admin.py for all discord message-containing tables (discordmessages, topmessages, gardenmessages)
    - [ ] G: add column for just string of hobs#1234 style discord username of message author (similar to top messages and garden messages)
    - [ ] G: **optional**: GardenMessage and TopMessage table custom views
- [ ] G: hacktoberfest: work on getting the tag processing feature integrated into a build of PyCord either for ourselves or for the public
- [ ] Brad/Gideon to create airtable public web page which can be embedded as iFrame in another app
- [ ] R: embeddable Svelte frontend widget for garden table

## Awaiting PyCord implementation of API

- [ ] G: forum posts: Add field to garden message table contain comma or newline-separated **tags** similar to the unique emojies string field currently in the table
- [x] G0.5-0.1: Editable field for changing the forum post tags

## Low Priority

- [ ] G: ping Gideon for any ideas he has for features needed before official launch mid October

## Retro Ideas

- [x] G0.5-0.1: improve the sub-bullet markdown rendering in gitlab for all current markdown docs

## Web design (optional)

- [ ] G: improve the design/appearance of the CS[m] log (white on black or better scaled image) - ping Shahar and Ronen about original artwork for logo that they want displayed and ask whether they would like white on dark or if they have white on dark.
- [x] G0.5-0.2: have top messages and garden messages tables in separate views
- [x] G0.5-0.1: move the contact info and compnay info to an About page
- [ ] G2-2: work with rochdi or just peruse other beautiful easy-to-use and common (familiar) webapps for design improvements
  * Made a fixed column header
  * Update button spacing
  * Removed 'Update list' button (because it's done automatically)
  * Add TEC logo to the about page
  * Add a footer with TEC logo and TEC, Common Sense[makers], and DAOstack copy
- [x] G0.5-0.2: ping Ronen and Shahar about adding TEC logo to about page and home page for non-admin users
```
    **IS**: exported csv does not contain all date timestamps (e.g. last edited time is missing)
    **SHOULD_BE**: export all timestamp columns to csv 
```

## GitLab [Markdown](https://docs.gitlab.com/ee/user/markdown.html)

To make a nested list, it's necessary to put the sublist under the first character of the list item's content.  For example, the bullet for '* Step 1' is under the first bracket '[', and the bullet for '- a' is under 'S'.

Example Nested List
- [x] Completed task
  * Step 1
  * Step 2
  * Step 3
- [ ] Uncompleted task
  * Step 1
    - a
    - b
    - c
  * Step 2
  * Step 3

Code
```
Example Nested List
- [x] Completed task
  * Step 1
  * Step 2
  * Step 3
- [ ] Uncompleted task
  * Step 1
    - a
    - b
    - c
  * Step 2
  * Step 3
```




## Backlog

- [ ] Add database table mapping discord user to twitter user
- [x] Add twitter handle database field to garden message of the discord author while keeping the field edditable

- [x] add export new garden messeges CSV file button - export garden messages without an export timestamp
- [ ] Guild custom emoji image download and save as binary blob in database
- [ ] Display custom guild emojie image(s) in stigsite table views (separate field/column?)

- [x] Bug: Rules table is broken in admin interface
- [x] Add unique users > 5 rule to rules table as data so that admin user can change threshold value
- [x] add field gardenmessage table for message type: [thread reply, or inthread post, or normal discord message]
- [ ] Server(Guild) + Channel pair as field for GardenMessage table
- [ ] Add list of discord user names filter for general access 
- [ ] Add filter on auth for a server

