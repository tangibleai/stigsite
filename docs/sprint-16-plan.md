# Sprint 16 (Nov 23 - Nov 30)


## Sprint 19
- [ ] make sure it's general so multiple rules can be active

- [ ] indicate whether staff or regular user/member on user profile page


* [] G16: Migrate data to central database
  - Fill django_site_id for each record in TEC (DM, TM, GM tables)
  - Download database data
  - Insert database data into unified database
* [] G2: Run through a full test sequence on the site/bot capabilities



## Notes
- [ ] Make user profile page prettier
- [ ] Make community profile page pretty
- [ ] Community Preferences page (staff) with way to enter twitter handles for each discord handle
- [ ] Community remove menu item for Garden Messages
- [ ] Incorporate threshold rule into community (numerical value)
- [ ] Incorporate threshold rule into community (numerical field (column) name)
- [ ] Incorporate threshold rule into community (numerical field (column) name)

* [] H?: Make Rochdi an administrator on `qary` discord server
* [x] R0.5-0.5: Install the project dependencies and create a new branch
* [x] R0.1-0.1: Set up new accounts on Airtable and Supabase using my Tangible AI email address
* [x] R0.1-0.1: Go to `docs/deploy.md` and copy the `.env` file example
* [x] R0.2-0.2: Create a new Supabase database
* [x] R1.5-2: Add these [tables](https://gitlab.com/-/ide/project/tangibleai/stigsite/edit/main/-/docs/deploy.md#set-up-supabase-tables) to the newly created Supabase database
* [x] R0.5-0.2: Create a new Discord bot called **purpledao**
* [x] R0.5-0.2: Create a Discord Identity Application for the newly created bot
* [x] R1-0.8: Create a new Airtable base and add these [fields](https://gitlab.com/-/ide/project/tangibleai/stigsite/edit/main/-/docs/deploy.md#set-up-supabase-tables) to it
* [x] R0.5-0.8: Make sure the `.env` file contains the necessary credentials for running `stigsite` properly
* [x] R0.2-0.1: Connect the newly created bot to the localhost server for easy testing
* [x] R0.1-0.1: Migrate and run the server


## Greg

* [x] G4-3.5: Rework community profile onboarding sequence
  - On joining, a user should get a blank Site object
  - On joining a user should get a blank Community Profile object
  - User should be able to see their own community profile on button click
  - The CommunityProfile object should be FK to the User
* [x] G8-4: Change community profile form
  - Add community_name field to the form (CharField)
  - Remove django_site_id field from form (not from model)
  - When a user edits CommunityProfile.community_name:
    * check that the name is unique among all community profiles (regular + lower())
    * if unique, update name in CommunityProfile and associate Site
    * if not unique, throw an error in view and do not save
