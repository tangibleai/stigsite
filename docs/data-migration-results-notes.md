# Data Migration Status

## Overview
Throughout the project, each instance of Stigapp used its own database.  With the integration of the Django Site framework, multiple instances of Stigapp can now rely on one database.  As a result, we wanted to consolidate data from multiple Supabase database instances to one instance.

## Issues to consider
1. Prior to the migration, Stigbot attempted to backfill `guild`, `channel`, and `django_site_id` in messages that were sent before these fields were added.  Sometimes, migrated database entries still have NULL values for these fields.  Messages sent during Video conferences or deleted messages could not be updated.  The effect is that these messages will not appear in Stigapp because they do not have a `site_id`.

2. The TEC Stigapp instance is still using its own database, but the data was migrated to the Dogfood database.  The TEC Stigapp's database settings need to be updated to use the Dogfood database.
    - Effect 1: New messages and interactions after Dec 12, 2022 may be lost when the database values are updated.
    - Effect 2: When the database values are updated, TEC user accounts will need to be remade.  This means when a TEC user logs in the first time, Stigapp will make them a new account.
    - Note: Once database values are shifted over, Stigbot will update message data for all fields of an entry during `on_raw_reaction_add` or `on_raw_reaction_remove` (add/remove reactions) for all tables, including Airtables.  This means existing Discord data will not be lost.
    
3. The `SITE_ID` in settings.py governs which `django_site_id` the views filter.  For example, if example.com's `Site` model `id` is 34, the `SITE_ID` would need to be set to 34 to filter these messages into the views.
    - Effect 1: It may be necessary to make this dynamic by creating multiple settings.py files.
    - Effect 2: It may necessary to relabel the `django_site_id` values on message data based on the `site_id` associated with particular community members.

4. `log_discord_message` may need to dynamically set the `django_site_id` for messages instead of using hardcoded values.
