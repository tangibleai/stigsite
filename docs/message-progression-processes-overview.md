
# Message Promotion Processes - DiscordMessage to GardenMessage

## Overview
Community interactions through Discord are hard to keep track of.  The entire community can benefit when interactions of interest are made transparent, regardless of platform-specific limitations/presentations.  Stigbot listens for and records interactions within a Discord community.  Stigsite presents this saved interaction data, and it also allows key community members to view and even edit the interaction data.

This document reviews the key process involved in Stigsite and Stigbot working with interaction data.

## Processes
1. DiscordMessage
	* When a message is created or interacted with, Stigbot will record the message to the database.  All messages sent in a Guild/Channel Stigbot has access to are saved to the DiscordMessage table.  Stigbot also will save messages sent before it was active when a user interacts with the message via an emoji (not sending a reply).
	* The relevant functions for this are `on_message`, `on_raw_reaction_add`, and `on_raw_reaction_remove`.  
	* The data for each message, whether new or existing, is entirely rebuilt upon each interaction using the same function.  `build_general_message_object` creates a dictionary with all the data for all fields across all message models.  This complete message object is filtered in `build_message_object_by_table` to get the fields specific to whatever table(s) the data is saved to.
	* Twitter Handle Request
		* Stigbot listens for new messages via `on_message`.  When a new message is received, Stigbot will review the community settings for the Guild the associated with the message using `get_community_settings`.
		* If a community profile has a matching `monitored_guild`, Stigbot checks that community's `enabled_twitter_handle_request` setting.  `get_community_settings` will return a dictionary of settings that includes the boolean `enabled_twitter_request`.
		* **BUG?:** It seems like the `community_settings` dictionary is not being used to determine whether to use the feature or not.
		* While building the message data in `build_general_message_object`, Stigbot calls `get_twitter_handle`.  `get_twitter_handle` reviews the post already has a Twitter handle associated with their account in Stigsite.  If they don't, Stigbot will send a message to the poster via a private DM.
		* Stigbot will listen from a DM in `on_message`.  If it detects a DM, Stigbot will call `receive_twitter_username_through_dm`.  This will update the user's `twitter_handle` field with the response the user provided.
		* All posts by the user across a DiscordMessage, TopMessage, and GardenMessage tables will be updated with the user's Twitter handle.
		* Stigbot will only ask for a user's Twitter handle once, on the first message it receives from that user.  Subsequent messages will not trigger this again, whether they user has responded or not.

2. DiscordMessage to TopMessage
	* A TopMessage is a message that has achieved a certain number of reactions by unique users (ie., the threshold).  For example, two users who each add the same emoji would count as 2 for the threshold.  A single user who leaves 5 emojis on a post would count as 1 for the threshold.
	* The threshold is set in the `Rule` model.  Currently, a Stigsite user must create a rule named "reactions rule" and set the reaction threshold value.  Also, there can only be one "reactions rule", and this rule will be applied to all communities using Stigsite.  Later, the CommunityProfile will have a reaction threshold value and Stigbot will use this value (or a default of 5) based on the Guild a message is sent from.
	* In Stigbot (log_discord_messages.py), the reaction threshold is obtained from `get_reaction_threshold_value`.
	* Messages promoted to the TopMessage table will also remain in the DiscordMessage table.
	* The threshold rule can be updated in Stigsite.  Stigbot will automatically apply it to any future interactions, but it will not proactively review existing DiscordMessages or TopMessages.
 
4. TopMessage
	* TopMessages are generally created and updated in log_discord_messages `on_raw_reaction_add` and `on_raw_reaction_remove`.
	* Stigbot will automatically add any message that crosses the reaction threshold to the TopMessage table.  Conversely, Stigbot will automatically remove any message from the TopMessage table that falls before the threshold.
	* TopMessages appear in the Stigsite /messages page.
	* Stigbot will automatically update any TopMessage database entry's data during `on_raw_reaction_add` or `on_raw_reaction_remove`.

5. TopMessage to GardenMessage
	* GardenMessages are curated messages selected by admin members of the community because of the messages' quality.
	* An admin user can set the `added_to_garden` view to `True` in the Django admin interface for the relevant message in the TopMessage model.  Doing so triggers stigapp.signals's `create_or_update_garden_message`.
	* Toggling True/False repeatedly for `added_to_garden` will have no effect on an existing GardenMessage entry in the database.
	* Upon being added to the GardenMessage database, Stigsite will add an entry to the `reward_reaction_log` table with the `reaction_sent` field set to `False`.  When any emoji interaction triggers either `on_raw_reaction_add` or `on_raw_reaction_remove`, Stigbot will trigger a check of the `reward_reaction_log` table for any message that has a value of `False` for `reaction_sent`.  A flower emoji is added to each post, and `reaction_sent` is changed to `True`.

6. GardenMessage
	* Stigsite presents GardenMessages only in the admin view.
	* Stigbot will automatically update any GardenMessage database entry's data during `on_raw_reaction_add` or `on_raw_reaction_remove`.

7. Airtable
	* Stigbot will automatically update any TopMessage or GardenMessage entry in the TopMessage or GardenMessage Airtable's during `on_raw_reaction_add` or `on_raw_reaction_remove` in log_discord_message.py.
	* Stigbot calls `AirTableServiceSupabase()` from stigapp.airtable_integration_supabase to perform any create, update, or delete events.
	* The TopMessage Airtable entries will automatically be added/removed based on a message meeting the threshold criteria.
	* Any GardenMessage that is added to the GardenMessage Airtable will not be deleted, even if it is deleted from the Supabase database.
	* The Airtable fields are hardcoded in the `AirTableServiceSupabase()`, so the Airtables need to be set up properly for the information to be recorded.