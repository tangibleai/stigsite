# Site Framework Implementation and Queries

## Site Overview
The Site framework is a component of Django that allows for whitelabelling of web applications.  When it is activated, it adds a model to the web application.  Represented as `django_site` in Stigapp's Supabase, the `Site` model contains 3 fields - `id`, `domain`, and `name`.  `domain` is the web address without "www" or "http" included (e.g., example.com).  The name is an arbitrary descriptive name for the site (e.g, example.com or Example, LLC).  Essentially, the Site framework makes it possible for a website to use the same codebase and database while changing the appearance and data of the site based on the `site_id`.

## Stigapp Models
The base `Site` model is included in Django, so it is not explicitly written out in any models.py.  Reference to the `Site` model can be made by importing `from django.contrib.sites.models import Site`.

Other models must be connected to the Site framework in order to use the framework's features.  This is done throughout the application by adding one or two fields.  `django_site_id` is necessary.  `on_site` is only necessary when one wants to automatically do the filtering based on `django_site_id`.

1. `django_site_id` provides a `ForeignKey` relation between a model and the `Site` `id`  field.  In Stigapp, all instances of these are called `django_site_id`.  The `related_name` attribute with `+` value will prevent the `Site` model from having a backwards relationship with the model using `django_site_id`.
```
    django_site_id = models.ForeignKey(
        Site,
        db_column='django_site_id',
        related_name="+",
        on_delete=models.CASCADE,
        null=True
    )
```

2. `on_site` adds a [CurrentSiteManager](https://docs.djangoproject.com/en/4.1/ref/contrib/sites/#the-currentsitemanager) to the model, which allows for automatic filtering of database data based on the `SITE_ID` in settings.py.  When a database entry's `django_site_id` matches the settings.py `SITE_ID`, Django will display or use that data.  This field is not necessary to use the Site framework, nor to query the database.  However, it simplifies both of these by automating the process.  Using it, though, requires a change to database queries, specified in the following section.
```
on_site = CurrentSiteManager('django_site_id')
```

The tables below show the database tables connected to the `Site` model and which of the two fields above they use.  In Stigapp, all message tables use `on_site` so that the views automatically message content in tables.

|Table Name | Location | Model | Site Fields |
|---        |---          |---        |---        |
| discord_message | stigapp.models | `DiscordMessageBase` | `django_site_id`, `on_site` |
| top_message | stigapp.models | `DiscordMessageBase` | `django_site_id`, `on_site` |
| garden_message | stigapp.models | `DiscordMessageBase` | `django_site_id`, `on_site` |
| auth_user | discordlogin.models | `User` | `django_site_id` |
| community_profile | discordlogin.models | `CommunityProfile` | `django_site_id` |


## Querying Models
The Site framework allows for one to write queries that automatically filter content based on the `Site` `id`.  However, the Site framework uses different queries depending on if a model uses `on_site` or not.  

Without the `on_site` attribute, one can query a model as normal.  

`auth_user` users the Site framework, but it does not use `on_site`.  Thus, we can use regular queries when interacting with it.
```
# discordlogin.auth
DiscordUser.objects.filter(id=user['id'])

DiscordUser.objects.create_new_discord_user(user)
```

`reward_reaction_log` does not use the Site framework at all.  It is only used as a collection of messages that need to receive a reaction.
```
# stigapp.signals
RewardReactionLog.objects.create(
    created_at=time_info,
    message_id=message_id,
    message_type=message_type,
    reaction_sent=False
)
```
**NOTE:** It is possible that this model may need to change and use `Site` attributes as Stigapp and Stigbot evolve.

When a model uses the `on_site` attribute, the query will need to use `on_site` instead of `objects`.

```
GardenMessage.objects.all() # This will fail, because the model uses `on_site`

# stigapp.views
GardenMessage.on_site.all()
```

We can still create complex queries using even when using `on_site` with a model.

```
# stigapp.views
GardenMessage.on_site.filter(
	message_id=message.message_id
).update(
    exported_at=export_time
)

# stigapp.signals
GardenMessage.on_site.get_or_create(
	....
)
```


## Resources
[Django's Site Framework Documentation](https://docs.djangoproject.com/en/4.1/ref/contrib/sites/)
