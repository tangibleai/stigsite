# Sprint 17 (Nov 30 - Dec 6)

## Rochdi

* [x] R0.5-0.5: hamburger (_=_) menu instead of ["Menu"]
* [x] R0.1-0.1: Change the name **Top Messages List** to **Messages**
* [x] R0.5-1: Make yourself an admin user
* [x] R0.2-0.2: Make sure the **Admin** is not included in the menu for the non-staff users
* [x] R0.2-0.1: Move the **community preferences** button moved to the hamburger menu
* [x] R0.1-0.1: Hide **Garden Messages** in hamburger menu and leave the `urls.py` and `views.py` unchanged
* [x] R0.1-0.1: Redirect the authenticated users to the home page
* [x] R0.1-0.1: Redirect the authenticated admin users to the **Top Messages** page
* [x] G: Community profile guilds list is used to decide whether to reply on those discord guilds for requesting Twitter Handles
* [x] G1-1: provide instructions text in either community profile template.html or to Rochdi as text or as docs/community_profile.md

* [x] G: community member list not implemented on backend
* [x] R0.1-0.1: hide the **community members** list field so it doesn't confuse the user
* [x] G: monitored channels are not implemented on backend
* [x] R0.1-0.1: hide the **monitored channels** list field on the community profile page
* [x] R1-1: Simplify and improve the navbar look
* [x] R1-0.8: Improve the home page design
* [x] R1-1: Improve the profile page design
* [x] R0.5-0.5: Force the footer to the bottom of the page
* [x] R1-0.5: Add a search filter to the messages table
* [x] R0.5-0.5: Improve the about page design
* [] R1: Make the community profile prettier and simple to use
* [] R: enable guild ids list used to filter `log_discord_messages.py` during checking of the threshold rule (and all other rules)
* [] R1: fancy low priority feature: try to find out which discord servers that the bot has been invited to by this staff user on Stigsite associated with this Community Profile
* [] R1: Community profile threshold connected to log_discord_message.py (change `get_reaction_threshold_value()`  function to use Django query instead of Supabase query)
  x Query community profile for Discord user id
  x Query the Pycord API with user id to find Discord servers associated with that user id
  x Save that list of servers (guild) in the community profile
* [] R: stigsite-tec(dogfood).onrender.com -> stigflow.onrender.com domain name in render.com dashboard


## Backlog (Sprint 18?)

- [ ] R: change threshold interface in community profile page to be a Zapier-like sentence:
  If message has {pulldown_condition=[">", ">=",...]} {number_field_threshold=[0,1,2,...]} {pulldown_action=["unique reactions", ...} then {pulldown_action="send to airtable"}.


## Backlog (Sprint 19?)
- [ ] make sure the rule configuration system is general enough to allow list of rules and some can be turned on or off and rules can be added
- [ ] indicate whether staff or regular user/member on user profile page



## LOW PRIORITY

- [x] G: monitored channels are not implemented on backend 
  - [ ] MED PRIORITY: R: enable filtering of log_discord_messages monitored using this community profile setting (list of channels)
  - [o] MED PRIORITY: R: improve UX for community profile guilds list by polling discord server for channels and create pull down of 
  - [o] R: add instructions and better UX for selecting monitored channels 
- [x] G: is community member list implemented on backend?
  - [ ] LOW PRIORITY: R: enable filtering of twitter handle requests and access to settings and Messages page
  - [o] LOW PRIORITY: R: add instructions and better UX for selecting community members 

### Greg

* [x] G1-0.5: Rewrite user profile form
* [x] G1-2: remove `.env` variable that disables Twitter handle request discord messages (localhost, dogfood, production/main)
  - [x] Remove .env variable links
  - [x] Re-enable the Twitter functions
  - [x] Test that the Twitter function is working based on CommunityProfile settings
* [] G16-8.5: Migrate data to central database
  - [x] Fill django_site_id for each record in TEC (DM, TM, GM tables)
  - [x] Download database data
  - [x] Write a script to update discord_message data to the database
    * Script 1: CSV > Supabase (via Supabase API)
    * Script 2: Supabase > Supabase (via Django ORM)
  - [x] Write a script to update the top_message/garden_message databases
    * Script 1: CSV > Supabase (via Supabase API)
    * Script 2: Supabase > Supabase (via Django ORM)
  - Insert DM database data into the unified database
  - Insert TM and GM data into the unified database
  - [] Copy over Airtable entries (remove any duplicate entries)
* [x] G1-0.5: Clean up database code in settings.py and models.py
* [] G2: Run through a full test sequence on the site/bot capabilities
  - [] Update the tests with new features added over the recent sprints



- [ ] download Discord profile avatar for the user profile on Discord and use in Stigflow
- [ ] download Discord guild avatar/logo/icon and replace CS[m] logo/avatar in community profile and user profile
- [ ] hamburger (_=_) menu instead of ["Menu"]
- [ ] redeploy stigsite.onrender.com

## Rochdi

- Be very careful with Discord (log_discord_messages.py)


## Backlog



## Backlog tech debt (move to MOIA)

- [ ] conversation (flow id) (file path) to the database and link it to both Trigger and State
- [ ] modularize the load_yaml.py file so it can load other flows from the command line
