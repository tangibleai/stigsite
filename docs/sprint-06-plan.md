# Sprint-06-plan

Sept 13 - Sept 20

#### infrastructure
- [x] G8-4: IN-PROGRESS: draft `deploy.md` documentation in docs/: create list of tasks for deploying copy of entire system stigbot + stigsite on render and configuring discord servers (adding private/bottest channel, webhook user, inviting webook and stibot users to server+channels).
- [x] G0.5-0.1: access control on discord login so that only superusers see export button and GardenMessage table view
- [ ] optional research: within django shell convert a discord user to a superuser
- [x] G0.5-0.1: allow hardcoded list of discord userid's to automatically be converted to superusers upon login
- [x] users can log in with discord and admins can designate them as superusers (admins) so that they can **only** click on the Admin button to access admin page (NOT by visiting /admin/ url directly).
- [x] G1-0.2: Database updates
    * deploy stigsite while retaining User data
    * User, GardenMessage, Rule(TBD) data in supabase
    * Add
- [x] G1-0.5: in GardenMessage table create an editable_message field and original_message field that is read-only within the admin interface
  * Create original_message and editable_message fields in models.py for TopMessage and GardenMessage (Using base class for original_message)
  * Update Supbase top_message and garden_message with new fields
  * Update create_leaderboard_data script with new fields
  * Update admin.py with new fields
  * Make original_message field readonly
  * Update CSV with new fields
- [x] G0.5-0.2: in GardenMessage table populate editable_message field with copy of message text when that row/record is created in the table.
- [x] G2-1: create home page ugly (unstyled) view that includes read-only table of garden message table (similar to admin view, but not sortable/editable, etc) with export CSV button
  * Add an export button for GardenMessage (as opposed to TopMessage)
  * Create a listview for GardenMessage
  * Add TopMessage to the view (through context)
  * Make a table for GardenMessage
  * Make a table for TopMessage
- [x] (Shahar did it live) send instructions on how to export CSV

- [x] G?-24: Fix bugs
  * Data columns values
    - [x] G: bug: user list (for reactions) should not be Nones > Problem: User data isn't being capture in reactions
    - [x] G: Update GardenMessage and TopMessage with fields improvement specified below (auth home page [x], admin page [--not style, but fields], CSV exports [--not style, but fields])
    - [X] Bot flower emoticons are being counted in the TopMessage table
    - [X] Bot flower emoticons are being counted in the DiscordMessage table
    - [X] Update script is adding the same message to the TopMessage table (even if it has the same id)
    - [x] Discord bot is adding duplicate emoticons to the discord_message table
    - [x] User count shows the wrong number of users (one user per entry instead of unique users)
    - [ ] Emojis that users clicked and then clicked again (removing) still counts towards the raw total
    - [ ] Replace signals.py user_id values for both create and update functions
  * G8-4.5: [x] Data column styling
    - [x] G: Style the fields as specified below (auth home page [x], admin page [], CSV exports [x])
    - [x] G: bug: reaction count isn't total number of user+reactions but unique emojies
  * G13: Authentication
    - [x] G: Discord users cannot delete other Discord users or interact with the database for some reason, but Django admin user can (Foreign_key_constraint failed
    - [x] G: Fix admin edit and delete bugs

- [x] G5-2.5: Get everything working on main

- [ ] G-: limit (filter) it to  (server + channel category + channels -> threads (X)
    * channel category = workspace (example of a channel category)
    * server = TEC server / sensemaking (ie., guilds)
    * channel = stigsite.dev (dev channels)
    * Choose a server and a channel, go through list, populate the top message table(hardcoded list in a constants.py file) (discord server, discord channel - pairs)
      - additional filter in addition to the threshold
    * ADD hardcode-list of admin to constants.py > later Supabase table with form in Django

#### TopMessage table updates
- [x] G0.5 (see below): Update TopMessage script should add all messages that match prescribed filter rule (total reaction count > 4, data deleted before creation)
- [x] G0.5 (see below): Update TopMessage script should only add new messages that meet rule/filter threshold and not remove any that have fallen below, nor change any edited table entries.
  * Data saved in DiscordMessage > TopMessage > GardenMessage
  * Data from DiscordMessage doesn't reflect when emoticon is taken away, so that's not reflected in upstream databases

- [X] G1-1: Update TopMessage script (This does the above two things)
  * Remove delete() function
  * Remove limit of [:10] to get the top 10 only
  * Add conditional to check if a threshold has been reached (hardcoded)
  * If conditional is met, get_or_create record
  * If get, nothing (no delete, no update)
  * If created, nothing (done)
  * Test

- [x] G3-1: Django view with button next to export csv, to update TopMessage table (delete and rebuild table)
- [ ] G8-: 5 min celery beat task to run TopMessage filtering/copying script (currently only run on deployment

- [ ] research possibility of listening to supabase/Postgres signals when Garden table is updated using supabase python API (supabase client), search for ("supabase callback", or "postgres signal")
- [ ] realtime TopMessage table
    * using `on_reaction_add` coroutine in log_discord_message.py (stigbot)
    * OR using channels-discord
    * OR create separate Django ORM used within log_discord_message.py (stigbot)
    * OR hacky workaround

## Backlog
- [ ] H: Fix  None, None, None users in log_discord_message.py and Supabase DiscordMessage(?)
- [ ] H: Clean up old branches
- [ ] H: merge to main and/or delete feature-modularize and feature-django-orm 
- access control on discord login  (discord user id)
- access control on discord login  (server=guild + role rules)

#### stigbot <-> django app communication
- test communication channel/signl using channels-discord or other side channel through supabase
- ? make TopMessage table real-time

#### Garden export (end users)
- [x] view for garden's export CSV where the export button
- [x] in garden column with list of users: "hobs hobs shahar"
- [x] unique_users: "hobs shahar"
- [x] in garden column string: ":-) ;) :-)"
- [x] in garden column all emoji-reactions count: 3
- [x]in garden column unique emoji count: 2 (unique emoji count)
- in
sum (len of string or reaction count)

#### Optics/Polish
- [x] Change "Django administration" text to "Stigflow Admin"

## Backlog

#### Extra feature
- real time
- [x] bot reacts to message (with flower) on discord when added to garden

## Architecture

#### Garden columns

- in garden column with list of users aligned with emoji list
- in garden column: string :-) :-)
- in garden column: sum (len of string or reaction count)

- string of [[':-)', 2]]

## user story
- they will export to csv
- import into airtable
- some process - meeting to discuss to the gardened message
- someone among gardenrs will copy/paste to twitter (or airtable automation)


- before launch: remove daotest from all channels
- before launch: invite to a discord channel for TEC

## Backlog

- attach (relation) context messages to garden/top messages
- multiplatform comments and messages attached to idea

