# Community Profile Information

## Description
The Community Profile stores the settings related to a particular community.  Both Stigapp and Stigbot will ultimately use these settings to work with the community and its data appropriately.

## Model
The associated model is at: discordlogin.models CommunityProfile

| Field | Type | Default | Purpose | Status |
|---    |---   |---      |---      |---     |
| id | int8 | PRIMARY | Act as a primary key and foreign relation refence      | Active |
| community_members | jsonb | NULL | Not defined: Maybe members Stigbot will listen to or members who have access to the community_profile | Inactive |
| community_name | varchar | NULL | The name the community goes by | Active |
| monitored_channels | jsonb | NULL | Discord channels the bot will listen to interactions on | Inactive |
| monitored_guilds | jsonb | NULL | Discord guilds the bot will listen to interactions on | Active |
| promotion_threshold | int8 | NULL | The number of reactions by unique people a post should get to be a TopMessage| Inactive (set in Rule table)|
| site_style | jsonb | NULL | A dictionary of attributes and values that govern a specific communities site | Inactive |
| django_site_id | foreign key (int4) | | Foreign key to the django_site table id field | Active |
| airtable_credentials | jsonb | NULL | A communities Airtable instance for receiving message interaction data | Inactive |
| enable_twitter_handle_request | boolean | false | Governs whether Stigbot will request a poster's Twitter handle | Active |


## Process
1.  Upon first authentication, Stigsite creates a User account, a Site, and a CommunityProfile.
    - User has a FK relation to Site through `django_site_id`
    - User has a FK relation to CommunityProfile through `community_profile_id`
    - CommunityProfile has a FK relation to Site through `django_site_id`

2. The User can update their profile, but not the `django_site_id`.

3. To update their CommunityProfile, they can click on `Community Preferences` from the menu or the button on their user profile.

4. When a user updates the `Community name` field in the CommunityProfile, Stigsite will update the Site `name` field and the CommunityProfile `community_name` field
   - NOTE: `Test1` and `test1` are evaluated as equivalent.  Two CommunityProfiles cannot have equivalent names.


## Currently Active Settings

The CommunityProfile form makes available fields for `Community name`, `Airtable credentials`, `Monitored guilds`, `Promotion threshold`, and `Enable twitter handle request`.  The form will save these values to the database.

Aside from the `Community name` and `django_site_id` fields as explained above, only two values are currently used to affect the system's behavior: `monitored_guilds` and `enable_twitter_handle_request`.

In `log_discord_messages`, `get_community_settings` checks the message against any value in the `monitored_guilds` dictionary.  If the server name is found in `monitored_guilds`, Stigbot will check the value of `enable_twitter_handle_request`.


## Extending the Community Profile System

- The `Airtable credentials` field needs to be split into four user-friendly form fields that save the data to the following json object:
  ```
  {
    "api_key": "test",
    "base_id": "test",
    "table_name_top_messages": "test",
    "table_name_garden": "test",
  }
  ```

- Once the above is done, `airtable_integration_supabase` (stigapp.airtable_integration_supabase) can be refactored to use these values based on the CommunityProfile setting when updating the Community's airtable.

- The `Monitored guilds` field needs to be a user-friendly field that saves the data into a JSON object:
  ```
  {
    "monitored": ['test1','test2','test3']
  }
  ```

- `get_community_settings` needs to work with these fields (`Airtable credentials` and `Monitored guilds`) above
- `promotion_threshold` needs to be evaluated in `log_discord_message`'s `get_reaction_threshold_value` function.  This will involve evaluating the setting from the CommunityProfile associated with the Guild of a message (if there is one)
