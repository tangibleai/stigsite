## Overview

Stigflow helps you surface the most engaging content from your Discord server.

## Prerequisites

You will need accounts on external sites to use Stigflow.

1. Create a new account on each of the following sites:

    * [Discord](https://discord.com/)
    * [Airtable](https://airtable.com/)

2. Ensure that you are the server administrator to which the bot will be invited. 

Access to the bot configuration and message data is granted only to Discord server administrators. 

## Sign up on Stigflow

After you meet the requirements above, you must complete the steps below to register on the Stigflow website:

1. Go to the [stigflow](https://stigsite-dogfood.onrender.com/) website.

2. Click on the Discord button **Join us** to create an account.

3. Next, hit the **Authorize** button to give `stigflow` access to your account.

4. Check the box after you read **Terms of Service** and **Privacy Policy**.

5. Click on **Agree** button to start using `stigflow`.

Once you agree, `stigflow` will record your acceptance to _Terms of Service_ and _Privacy Policy_. 

Stigflow will not be able to read or send messages on your behalf. 

## User profile

When you log in for the first time, Stigflow will redirect you to the **User Profile** page so you can enter your details. You must complete the following fields before proceeding: 

* **Discord handle** (this field is automatically filled after you sign up)
* **First name**
* **Last name**
* **Email** (make sure your email address exists and active)

Click on the **Save** button to save your profile information. Also, you can cancel your changes with the **Cancel** button on the left.

## Community preferences

After you edit your _user profile_, and connect your Discord account to Stigflow, go to the **Community Preferences** page from the top right menu to edit your community profile. 

The **Community Preferences** form contains the fields required to connect Stigflow to your Airtable account with `stigflow`:

* AIRTABLE_API_KEY
* AIRTABLE_BASE_ID
* AIRTABLE_TABLE_NAME_TOP_MESSAGES

Here's what you need to do to retrieve this information from your Airtable account:

1. Log into your Airtable account.

2. Go to the top right of the navbar, and click on the profile image.

3. Select the first option in the menu **Account**.

4. Scroll down and click on **Generate API key** under the API section.

5. Copy the API key and paste it into the **API key** field under the **Airtable credentials** section in the **community preferences** page.

6. Go back to your Airtable home page, and create a new Airtable base (give your base a convenient name).

7. The base will have an Airtable URL like this example below, copy the portion of the URL that starts with _app_ (ie., "appbDrod2abrajcNS"). Then, paste it into the **Base id** field under **Airtable credentials**section on the **community preferences** page.

```
https://airtable.com/appbDrod2abrajcNS/tblsdJYFSJclFAHAA/viwDjfshEw4gHDhDUq?blocks=hide
```
8. One table you need to visualize your community data and surface the most engaging content. Within your Airtable base, click on **+ Add or import** and select the **create blank table** option.

9. Add the following fields to your table, and note the capitalization and spacing of each field name matters:

| Field Name | Type | Additional Settings |
|---         |---   |---                  |
| Author | Single line text | NA |
| Twitter Handle | Single line text | NA |
| Guild | Single line text | NA |
| Channel | Single line text | NA |
| Message ID | Single line text | NA |
| Message Type | Single line text | NA |
| Thread Title | Single line text | NA |
| Original Message | Long text | NA |
| Authorship | Date | ISO, Include time, 24 hour, Display time zone |
| Editable Message | Long text | NA |
| Edited At | Date | ISO, Include time, 24 hour, Display time zone |
| All Reactions | Number |  Integer |
| Unique Reactions | Number | Integer |
| All Emojis | Single line text | NA |
| Unique Emojis | Single line text | NA |
| Totals by Emoji | Single line text | NA |
| Total Users | Number |  Integer |
| Unique Users | Number |  Integer |
| Total User List | Single line text | NA |
| Unique User List | Single line text | NA |
| Exported At | Date | ISO, Include time, 24 hour, Display time zone |
| Extracted URL | URL | NA |
| Jump URL | URL | NA |
| Added to Garden | Checkbox | NA |

10. Give the table a descriptive name (eg. **TopMessages**). Then, copy and paste the name into the **TABLE_NAME_TOP_MESSAGES** field on the **community preferences** page.


You can edit other settings from the **community preferences** page, like the **name** of the community and **Promotion threshold**. After you fill out the necessary credentials, the next step is to choose the guilds or servers you want for `stigflow` to monitor. To do so, go to the **Monitored guilds** field and type the server/guild name. Then, press Enter to add the guild. Also, the **Monitored guild** field allows you to add multiple server names at once, all you need is to add a comma after every server name.

After that, you can save your community profile information by clicking the **Save** button. Additionally, the **Cancel** option on the left allows you to undo your modifications. 

## Monitor your Discord community

You are now prepared to begin keeping an eye on your Discord community and locating the most intriguing information. You may examine your community data in the **messages** page from the top right menu of your **stigflow** site or you can access your community insights from the Airtable table you generated in the preceding steps. As soon as something happens on the registered servers, these pages will be updated.
