# Setting up a Discord Identification Application

Discord Identification Applications only allow one redirect url.  It is recommended that you go through the steps below to make one identification application for your local environment and one for the production environment.

1) Log in to [Discord](https://discord.com/login).

2) Go to the [Discord Developer Portal Applications](https://discord.com/developers/applications).

3) Click *New Application*.

4) Give the chatbot a name.  Click *Create*.

5) From the sidebar, click **OAuth2**.

6) Click **Reset Secret**.

7) Under "Client Secret", click **Copy**.  Save the client secret in the .env file as DISCORD_LOGIN_CLIENT_SECRET.

8) Under "Client ID", click **Copy**.  Save the client ID in the .env file as DISCORD_LOGIN_CLIENT_ID.

9) Under "REDIRECTS", click **Add Redirect**.

10) Enter the redirect for both localhost (ie., http://localhost:8000/oauth2/login/redirect).  Click **Save Changes** when finished.

9) Select **URL Generator**.

10) In SCOPES, select **identify**.

11) Enter one redirect URL for the SELECT REDIRECT URL (Example: http://localhost:8000/oauth2/login/redirect).

12) Under GENERATED URL, click **Copy**.

13) Save the generated url in the .env as DISCORD_LOGIN_AUTH_URL.
