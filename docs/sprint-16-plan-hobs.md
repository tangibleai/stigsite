## Sprint-16

- [ ] Make user profile page prettier
- [ ] Make community profile page pretty
- [ ] Community Preferences page (staff) with way to enter twitter handles for each discord handle
- [ ] Community remove menu item for Garden Messages
- [ ] Incorporate threshold rule into community (numerical value)
- [ ] Incorporate threshold rule into community (numerical field (column) name)
- [ ] Incorporate threshold rule into community (numerical field (column) name)