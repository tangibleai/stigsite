# CHANGELOG

## [0.0.17](https://gitlab.com/tangibleai/stigsite/-/tags/0.0.17)

Updates
- Django sites framework has been implemented in all message tables (discord_message, top_message, garden_message)
- Views (site pages) filter data based on the `SITE_ID` in settings.py
- User onboarding now produces both a unique Site entry and a unique CommunityProfile entry for each user
- Views (site pages) has been redesigned to improve UX and general appearance.  More revisions are forthcoming
- Refactor the Stigbot to fix errors and unify the approach to message logging across on_message and on_reaction functions.
- Update test sequence with new capabilities

Notes
- Contains a working implementation of two separate databases
- Contains commented out routing code for working with two databases, including:
  * stigapp.routing - A file that specifies when the app should use a specific database
  * stigapp.settings - Code to enable the routing file (`DATABASE_ROUTERS`)
  * Note: `_DATABASE=supabase` has been deleted from all models, but this is necessary for working with the routing


## [0.0.14](https://gitlab.com/tangibleai/stigsite/-/tags/0.0.14)

Updates

- Updated navigation bar with condensed menu
- Added relationship between discord_message and site models
- Added form for community profile settings
- Added a capability to turn on/off Twitter handle requests from Stigbot


## [0.0.13](https://gitlab.com/tangibleai/stigsite/-/tags/0.0.13)

New Features

- Stigbot will request Twitter handle from users who have not supplied one yet.
- Added initial support for supporting different versions of Stigsite through the Sites framework
- Stigbot will extract URLs from messages


Updates

- Fixed channel_name field value
- Added Forum/Thread title field


## [0.0.2](https://gitlab.com/tangibleai/stigsite/-/tags/0.0.2)

Updates

- Added authorship_data, guild_name, and channel_name to all tables and Airtables
- Updated reaction threshold to work with **greater than or equal to**
- Updated reaction threshold to work with **0** as a value
- Consolidated stigapp models.py files


## [0.0.1](https://gitlab.com/tangibleai/stigsite/-/tags/0.0.1)

Initial release

- User-defined rule for TopMessage table (threshold and emojies list)
- TopMessage/GardenMessage airtable live (real-time) sync
- BUGFIX: all reactions are monitored using on_raw_reaction_add() and on_raw_reaction_remove()
