# Your Customized StigBot

StigSite and StigBot will help you surface the most interesting content from your Discord Server.
You will first need to let us know who should have access to the Bot's configution data and "Top Messages" from your Discord Channels.

## Admin Users

Send us a list of the Discord handles (e.g. hobs#3386) and Twitter handles for all of the people you would like to have access to your Discord messages.
They will be able to export these messages to a CSV file from the StigSite home page (currently at https://stigsite.onrender.com).
They will also be able to configure the StigBot behavior and edit the message content within the StigSite admin pages.


## Create a Discord Bot

The person that creates the Discord Application (Bot) will be able to invite it to any channel where they are an administrator, so make sure you are an administrator on the channels the bot will need access to.

1. Visit https://discord.com/developers/applications
2. Select the blue button [New application]
3. In the modal dialog box that pops up, type in a name such as stigbot or TECbot or whatever name you would like your bot to have. And then click the accept EULA checkbox
4. Optionally type a description of the new bot

## OAuth2 -> General menu
1. Scroll down to find the "Application ID". Copy and paste it to this file with a variable ame DISCORD_APP_ID (SEE BELOW) =1023283171149758564
2. Scroll down to find the "Public Key". Copy and paste it to this file with a variable name DISCORD_APP_PUBLIC_KEY (SEE BELOW) =1023283171149758564
3. Ignore the optional "Interactions Endpoint URL", "Terms of Service URL", and "Privacy Policy URL"

## Bot menu -> Build-A-Bot section
1. Click the Bot menu item on the lefthand toolbar and then click "Add Bot" under "Build-A-Bot" and confirm after the "Choose wisely" warning.
2. Under Build a Bot click [Reset Token] and are you sure [Yes] and your TFA token from your authenticator app, if required.
3. Copy the new token with the blue [Copy] button next to "Reset Token".
4. Paste the Token into this .env file for the variable DISCORD_BOT_TOKEN
5. Make sure the "Public Bot" toggle is switched on (blue)
6. Make sure the "MESSAGE CONTENT INTENT" is switched on (blue)
7. Save your settings with blue button at bottom

## OAuth2 -> General menu
1. Click on the OAuth2 lefthand tool bar menu item
2. On the General submenu under OAuth2 click "Reset Secret" and "Yes Do It" and enter your Discord account TFA code if required
3. Notice the Secret Key is shown and will not be shown again so click the blue Copy button before leaving the page. Paste the Secret Key into this file as DISCORD_APP_CLIENT_SECRET
4. Under Default Authentication Link select the "Authorization Method" named "In-app Authorization"
5. Under "SCOPES" in the Oauth2 URL Generator (left toolbar), select "bot" Role/Scope
6. Under "BOT PERMISSIONS" -> "TEXT PERMISSIONS" Select 5: "Send Messages", "Create Public Threads", "Send Messages in Threads", "Read Message History", and "Add Reactions"
7. Select blue [Copy] button to right of "Generated URL"
8. Paste the Generated URL in your browser and authenticate your discord user
9. Copy the APP_ID, PUBLIC_KEY, PRIVATE_KEY to a .env file (private text file) like this one

#### _`.env`_
```bash
# STIGBOT
DISCORD_BOT_ID=1023329201262170122
DISCORD_BOT_TOKEN=REPLACEME-72-LETTERS-DIGITS-DOTS-DASHES

## Optional
DISCORD_APP_CLIENT_SECRET=REPLACEME-32-LETTERS-DIGITS
DISCORD_APP_PUBLIC_KEY=REPLACEME-64-LOWERCASE-LETTERS-DIGITS-FOR-HEXADECIMAL-BASE-16-INTEGER
```


## Create a Discord Channel Webhook
The webhook will send a message through a private channel to the Discord bot to add a flower emoji to a user's message.  An admin user must create this channel.

1. In the Discord server, click the + icon next to TEXT CHANNELS, select Text, set the CHANNEL NAME to "admin", and set the channel to private.  Click [Create Channel].
2. Click the gear icon next to the channel you created.
3. Click [Integrations], and then click [Create Webhook].
4. Name the webhook "bothook", and select the channel you created earlier (admin).  Click [Save Changes].
5) Select [Copy Webhook URL].
6) Save the webhook URL in the .env as DISCORD_WEBHOOK.
7) Save the channel name you set (admin) in the .env as DISCORD_WEBHOOK_CHANNEL_NAME.

#### _`.env`_
```bash
# WEBHOOK
DISCORD_WEBHOOK=REPLACEME-URL
DISCORD_WEBHOOK_CHANNEL_NAME=admin
```
