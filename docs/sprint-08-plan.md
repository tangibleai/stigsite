# Sprint-08-plan

## Ronen/Shahar Requests

- [x] G5-3.25: use keys sent by r-x-x-x to switch stigsite and log_discord_message to listen to TEC server instead of DAOStack CS[m]
  * [x] Monitor to see if the bot is picking up messages
  * [x] Fix bug with unique_user_count
  * [x] Split staging / main Supabase instances.  Main will only listen to TEC server.  Staging will listen to DAO server (for testing).

- [x] G0.5-0.1: bug: export all timestamps

- [x] G: research signaling of updates to a table from postgres/supabase database
  * [-] H1-.5: experiment with supabase realtime-api `.subscribe()` query method in Python
    * https://supabase.com/docs/guides/api#realtime-api
    * example: `async def on_garden_message_insert(*args, **kwargs): ... return results; ... await supabase.from('GardenMessage').on('INSERT', on_garden_message_insert).subscribe()` 
  * [-] H1-.5: experiment with [SQL NOTIFY and LISTEN](https://www.postgresql.org/docs/current/sql-notify.html) statements using supabase raw SQL using python supabase client

- [-] G: if direct database signaling not possible implement 1 min polling of database in log_discord_messages for notification of changes to the Garden message table (to react with flower)

- [x] G2-1: Twitter handle remembered and automatically updated or populated
- [x] G0.5-0.1: replace home page with Garden messages table (without top messages table)
- [x] G0.5-0.1: change garden message menu button label to "Top Messages"
- [x] G0.5-0.1: Make a page for the TopMessage table

- [x] G: FYI: user would like to export only messages they haven't exported and successfully imported to Airtable before
- [x] G: CUSTOMER REQUEST: filter messages exported based on export date (export all messages since the last export), table called Export with datetime column and filter_json_field
- [x] G: FYI: user may sometimes export csv files for testing or demo and do not want them to count as real exports that were imported to Airtable
- [ ] G: alternative ux allows the user to select all with a boolean field on each Garden Message entry for whether the message has been imported into Airtable
- [ ] G: CUSTOMER REQUEST: filter messages exported based on export date - export all messages since a user specified or selected datetime

- [x] G: Live update of Top Messages table on each reaction using it within the log_discord_messages.py (stigbot) and disable the 3 min update cron job
- [x] H: work with TEC dev "R-x-x-x" to create new bot and add it to TEC server keys sent to greg

## Awaiting PyCord implementation of API

- [ ] G: forum posts: Add field to garden message table contain comma or newline-separated **tags** similar to the unique emojies string field currently in the table
- [x] G0.5-0.1: Editable field for changing the forum post tags

## Low Priority

- [ ] G: adding infrastructure for Rules
```
    - create data structure in python that can be used to call hard-coded rule functions currently used to filter exports of csvs (see log_discord_messages.DEFAULT_ACTIONS)
    - add table Rule (fields like on_export, condition, threshold_value)
    - add table Action (fields: function name, function_args, function_kwargs) -- see log_discord_messages.DEFAULT_ACTIONS...
    with fields for 
```

- [ ] G: ping Gideon for any ideas he has for features needed before official launch mid October

## Retro Ideas

- [x] G0.5-0.1: improve the sub-bullet markdown rendering in gitlab for all current markdown docs

## Web design (optional)

- [ ] G: improve the design/appearance of the CS[m] log (white on black or better scaled image) - ping Shahar and Ronen about original artwork for logo that they want displayed and ask whether they would like white on dark or if they have white on dark.
- [x] G0.5-0.2: have top messages and garden messages tables in separate views
- [x] G0.5-0.1: move the contact info and compnay info to an About page
- [ ] G: work with rochdi or just peruse other beautiful easy-to-use and common (familiar) webapps for design improvements
- [x] G0.5-0.2: ping Ronen and Shahar about adding TEC logo to about page and home page for non-admin users
```
    **IS**: exported csv does not contain all date timestamps (e.g. last edited time is missing)
    **SHOULD_BE**: export all timestamp columns to csv 
```

## GitLab [Markdown](https://docs.gitlab.com/ee/user/markdown.html)

To make a nested list, it's necessary to put the sublist under the first character of the list item's content.  For example, the bullet for '* Step 1' is under the first bracket '[', and the bullet for '- a' is under 'S'.

Example Nested List
- [x] Completed task
  * Step 1
  * Step 2
  * Step 3
- [ ] Uncompleted task
  * Step 1
    - a
    - b
    - c
  * Step 2
  * Step 3

Code
```
Example Nested List
- [x] Completed task
  * Step 1
  * Step 2
  * Step 3
- [ ] Uncompleted task
  * Step 1
    - a
    - b
    - c
  * Step 2
  * Step 3
```




## Backlog

- [ ] Add database table mapping discord user to twitter user
- [x] Add twitter handle database field to garden message of the discord author while keeping the field edditable

- [x] add export new garden messeges CSV file button - export garden messages without an export timestamp
- [ ] Guild custom emoji image download and save as binary blob in database
- [ ] Display custom guild emojie image(s) in stigsite table views (separate field/column?)

- [x] Bug: Rules table is broken in admin interface
- [ ] Add unique users > 5 rule to rules table as data so that admin user can change threshold value
- [x] add field gardenmessage table for message type: [thread reply, or inthread post, or normal discord message]
- [ ] Server(Guild) + Channel pair as field for GardenMessage table
- [ ] Add list of discord user names filter for general access 
- [ ] Add filter on auth for a server

