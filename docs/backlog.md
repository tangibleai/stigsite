# Backlog

- [ ] add export new garden messeges CSV file button - export garden messages without an export timestamp
- [ ] Guild custom emoji image download to incorporate into table view of emojies (currently a str, but may need to be something else like list of images)


## Epic: Authentication

- Logout button/menu
- Django password reset
- Django signup button/menu
- Django signup page/form (email and password only)
- Django login with email+password (with discord login button on same page)
- Django form to edit user profile (avatar, name, bio, homepage url, discord profile url, interests/tags)

## Epic: User-configurable Bot Actions

Line 71 in log_discord_message.py: TODO:
- Create Django model class BotAction
    - BotAction.trigger_type = choices field ['message', 'reaction']
    - BotAction.func_name = varchar max_len 64
    - BotAction.func_args = jsonfield
    - BotAction.func_kwargs = jsonfield
    - BotAction.enabled = bool

## Epic: User-configurable Bot Rules

See backlog section for [sprint-7](sprint-07-plan.md#backlog)

- Create Django model class TopMessageRules

### Infrastructure
- H1: redis + uvicorn + pycord + ascync environment

## Don't Do

- G1: create fastapi demo site from tutorial
- G1: use huggingface python webapp package to create huggingface spaces 

## Rough ideas:

### Alternate approach for updating reactions_users_list and `str`

To record additional information that discord discards, we need to maintain seperate lists of dictionaries with the previous reacitons (emojies) and the user that created the emoji reaction.

Currently in log_discord_messages.record_reaction() we use the discord-provided reaction.message.reactions list of reaction counts to update our supabase message.previous_reactions list of dictionaries, but that overwrites the user information in that list of dicts. 

Use our own supabase table field message.previous_reactions to keep track of (users, emoji) pairs for each message and have (emoji, count) pairs in separate field called reaction_counts.

Maybe better field names would be reaction_user_list and reaction_count_list (so the outer pluralization in Django admin interface looks right)
Our supabase message.previous reactions listofdicts contains the user for each reaction instead of just count

### Discord Tags

Shahar Oriel: Hey @mariadyshel @Ronen Tamari @hobs , this is a cool new feature by Discord. Which the TEC would like to utilize for the curation service. This the forum is a channel which contains posts, which are threads with a top message. The bot can go into the forum and into the threads (I checked). Here is the edition I found in the API documentation - https://discord.com/developers/docs/topics/threads#forums
Would be great to see if we can pick-out the tags of posts

GideonRo: When you set up a forum, you can set custom tags as well as the default emoji. That's going to be very useful

