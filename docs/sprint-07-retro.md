# Retrospective feedback

## Issues to be aware of
### Custom emojis
Custom emojis do not seem to have an emoji string, only a name.  In order to replicate the emoji in Stigsite, we may need to use a similar emoji from a library like FontAwesome or purchase custom images.

This is why the site currently lists the DAOstack custom tweet emoji as :tweet:.

<table>
<tr>
<th>Regular Emoji</th>
<th>Custom Emoji</th>
</tr>
<tr>
<td>

```json
{
    'reaction': <Reaction 
        emoji='🎉' 
        me=False 
        count=1
    >, 
    'user': {...}
```
</td>
<td>

```json
{
    'reaction': <Reaction 
        emoji=<Emoji 
            id=1001468410418434200 
            name='tweet' 
            animated=False 
            managed=False
        > 
        me=False 
        count=1
    >, 
    'user': {...}
```
</td>
</tr>
</table>


### Forums / Forum tags
Being a relatively new feature, support for Forums is not yet complete in the package we are using.  Stigsite is using Py-cord, not discord.py.

Py-cord - [Forum tags not implemented](https://github.com/Pycord-Development/pycord/pull/1636)

discord.py - [Forum tags implemented](https://discordpy.readthedocs.io/en/latest/api.html?highlight=forum#discord.ForumChannel.available_tags)


### TopMessage Data
The TopMessage data is being populated three different ways.
1. A script (`create_leaderboard_data.py`) runs on deployment.
2. A button on /garden runs `create_leaderboard_data.py` to update the data.
3. The bot (`log_discord_messages.py`) listens for reactions.  Every time a reaction is added, the bot evaluates whether the message meets the threshold (unique users > 5) and either creates a updates a TopMessage entry.

## General feedback
### Communication channels
Sometimes, communication is split across various channels and updated periodically in different places.  These places have varied by week.

**Suggestion:** Commit to one channel for communication about where the prioritized sprint tasks live.  Make sure sprint tasks are clearly defined/contextualized there.


### Paid features
It can be difficult to test features behind a paywall, such as custom emojis or forums.

`dao-gardens-live-test` and `test-forum` have been useful.  However, it's slower to work there than on my private server I have full control over.

**Suggestion:** Allot or plan for more development time to account for additional research, experimentation, and testing.


### Adding vs. refining
Over the past few weeks, we have added features and capabilities for Stigsite/bot.  The app has evolved based on ideation and discussion.

Some technical debt has been built up due to the quick pace and changing requirements.  For example, the database tables might be reorganized based on the additional fields added over the last few sprints.

**Examples**
1. Database tables might need to be rethought now that more fields have been added.  Also, there may be additional fields that could make the application run smoother, such as channel info.
2. Flower emoji reward (on_reaction_add) scans every message on the server until it finds the right message/thread.
3. Is a webhook triggering the flower emoji reward the best approach?  Probably need to explore more.

**Suggestion:** It could be useful to allot some time to deal with the technical debt by developing tests and refining code.
