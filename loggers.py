"""

1. visit the discord developer dashboard and generate a URL for a bot with admin privileges (the permissions bit mask integer in the URL should be long).

2. start the discord bot service:

>>> python log_discord_messages.py
{...}
We have logged in as daotest#7533

3. Visit the discord server you generated the URL for and you should see your bot among the list of onine users.
4. In the online users list on discord right click the bot and invite them to the server.
5. In discord say "hello what's up?", lowercase hello is required, (on any public channel)?

You should see something like this logged in the terminal where the bot is running.
This is the message the bot posted on the server (because that's the only print statement in the on_message fun)
This is **NOT** the original message from the user:

Received message from bot (daotest#7533):
    Hello! You just said: hello what's up?
message_obj:
<Message
    id=1009925779129389138
    channel=<TextChannel
        id=992807246788567134
        name='general'
        position=4
        nsfw=False
        category_id=992807246788567133
        news=False>
    type=<MessageType.default: 0>
    author=<Member
        id=1007105836092497950
        name='daotest'
        discriminator='7533'
        bot=True
        nick=None
        guild=<Guild
            id=992807246113276064
            name='qary'
            shard_id=0
            chunked=False
            member_count=7
            >
        >
    flags=<MessageFlags value=0>
>


"""
from supabase import create_client
import config
import logging

from config import LOG_LEVEL, PRINT_LOG_LEVEL, BOTNAME

SUPA = create_client(config.SUPABASE_URL, config.SUPABASE_KEY)


def get_coroutine(obj, attr_path='channel.send'):
    """ Same functionality as getdottedattr? """
    coro = obj
    for attr in attr_path.split('.'):
        coro = getattr(coro, attr)
    return coro


def getdottedattr(obj, attr, default=None):
    """ Same functionality as get_coroutine? """
    child_obj = obj
    attr = attr.strip().strip('.')  # in case someone calls this with a an attr named '.whatever.child.name'
    children = [attr]
    if '.' in attr:
        children = attr.split('.')
    for name in children[:-1]:
        child_obj = getattr(child_obj, name, default)
    return getattr(child_obj, children[-1], default)


async def await_action(message, action='channel.send', *args, **kwargs):
    await get_coroutine(message, action)(*args, **kwargs)


def to_dict(obj, attrs):
    """ Convert an obj to a dictionary using getattr(obj, key, None) for each attr (key) in attrs """
    return {a.strip().strip('.').replace('.', '__'): getdottedattr(obj, a, None) for a in attrs}


def flatten_coroutine(obj, attr):
    return await_action(obj, attr)().flatten()
    # list(await reaction.users().flatten())


def to_listodicts(objs, attrs, coros=()):
    list_of_dicts = [to_dict(obj=obj, attrs=attrs) for obj in objs]
    return list_of_dicts
    # for coro in coros:
    #     for d, obj in zip(list_of_dicts, objs):
    #         d[coro] = flatten_coroutine(obj, coro)


def isoformat(dt):
    if dt is None or not dt:
        return None
    return getattr(dt, 'isoformat', lambda x: None)()


def log(text='', json='', log_level=logging.WARNING):
    if log_level > LOG_LEVEL:
        if log_level > PRINT_LOG_LEVEL:
            if text:
                print(text)
            if json:
                print(json)
        return SUPA.table('log').insert(dict(
            text=text,
            json=json,
            log_level=log_level,
        )).execute()


def log_at_log_level(*args, log_level=logging.ERROR, **kwargs):
    """ err_message is str(args[0]) (so we'll log malicious user with kwargs['err_message']=sqlinjectiontext)!!! """
    err_message = 'ERROR: Empty error message received in log_discord_message.log_error(*args, **kwargs).'
    if len(args):
        # TODO: AttributeError: 'tuple' object has no attribute 'pop'
        # err_message = str(args.pop(0))
        pass
    elif 'text' in kwargs:
        err_message = kwargs.pop('text')
    js_dict = dict(args=args, kwargs=kwargs)
    return log(text=err_message, json=js_dict, log_level=log_level)


def log_error(*args, log_level=logging.ERROR, **kwargs):
    """ Ignore log_level kwarg and only log message if logging.ERROR > config.LOG_LEVEL """
    return log_at_log_level(*args, log_level=logging.ERROR, **kwargs)


def log_warning(*args, log_level=logging.WARNING, **kwargs):
    """ Ignore log_level kwarg and only log message if logging.WARNING > config.LOG_LEVEL """
    return log_at_log_level(*args, log_level=logging.WARNING, **kwargs)


def log_debug(*args, log_level=logging.DEBUG, **kwargs):
    """ Ignore log_level kwarg and only log message if logging.DEBUG > config.LOG_LEVEL """
    return log_at_log_level(*args, log_level=logging.DEBUG, **kwargs)


def log_info(*args, log_level=logging.INFO, **kwargs):
    """ Ignore log_level kwarg and only log message if logging.INFO > config.LOG_LEVEL """
    return log_at_log_level(*args, log_level=logging.logging.INFO, **kwargs)
