# import os
from dotenv import load_dotenv, dotenv_values

# creates os environment variables from the .env file
load_dotenv()

CONFIG = dotenv_values()

globals().update({
    k: CONFIG.get(k, k) for k in (
        # database for both log_discord_messages.py and stigite Django app(s)
        'SUPABASE_URL',
        'SUPABASE_KEY',

        # used by log_discord_messages.py script using py-cord async websockets API
        'DISCORD_BOT_ID',
        'DISCORD_BOT_TOKEN',

        # used to communicate with log_discord_messages.py service about garden message flower reactions
        # 'DISCORD_CHANNEL_ID',
        # 'DISCORD_WEBHOOK',
        # 'DISCORD_WEBHOOK_CHANNEL_NAME',

        # used for authentication with Discord account
        'DISCORD_LOGIN_AUTH_URL',
        'DISCORD_LOGIN_CLIENT_ID',
        'DISCORD_LOGIN_CLIENT_SECRET',

        # used for Airtable integration
        'AIRTABLE_API_KEY',
        'AIRTABLE_BASE_ID',
        'AIRTABLE_TABLE_NAME_TOP_MESSAGES',
        'AIRTABLE_TABLE_NAME_GARDEN',


        # General Django Settings
        'TIME_ZONE',
        # 'ENABLE_TWITTER_HANDLE_REQUESTS'
    )}
)


PRINT_LOG_LEVEL = 10
BOTNAME = 'stigbot'
LOG_LEVEL = 10

DEFAULT_MESSAGE_ACTIONS = (
    # TODO: change these default args to None, and then allow the 3-tuple to be a 1-tuple and then just a str (without messing it up)
    ('record_message', (), {}),
    ('add_reaction', ('✔',), dict(condition=dict(regex=f'\\b{BOTNAME}\\b'))),
    # ('add_reaction', ('✍️',), {}),  # 🛢  💾  📓  📄  📒  🏷   ✔  ✅  ✴  ✳  🔹  ◼  ▫
    # ('reply', ['noted'], {}),
    # TODO allow format string templates with `def render_fstring(fstring, message):`
    # ('channel.send', ['noted'], {}),
)

DEFAULT_REACTION_ACTIONS = (
    ('record_reaction', (), {}),
    # ('add_reaction', ['✔'], {}),  # ✍️  🛢  💾  📓  📄  📒  🏷   ✔  ✅  ✴  ✳  🔹  ◼  ▫
    # ('reply', ['noted'], {}),
    # TODO allow format string templates with `def render_fstring(fstring, message):`
    # ('channel.send', ['noted'], {}),
)

# Python function names that user's can specify as actions when new messages, reactions, or users are received from discord
ALLOWED_FUNS = 'record_reaction record_message insert_supabase add_reaction reply channel.send'.split()
