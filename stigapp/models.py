from django.db import models
# from django.db.models.signals import post_save
from django.utils import timezone
from django.contrib.sites.models import Site
from django.contrib.sites.managers import CurrentSiteManager
# from stigapp.models_discord_supabase import *  # noqa


##############################################
# TBD placeholders for local sqlite records (leaderboard, etc)

# class Author(models.Model):
#     _DATABASE = 'default'


# class Reaction(models.Model):
#     _DATABASE = 'default'


# class Message(models.Model):
#     """ A table for messages the bot can send.
#     Each row contains one message written in one language.
#     """
#     _DATABASE = 'default'

#     content = models.TextField(
#         null=False,
#         help_text="The message of the state written in English"
#     )

#
################################################################
# Discord message models

class DiscordMessageBase(models.Model):
    """ An abstract class that contains all the common fields between DiscordMessage, TopMessage, and GardenMessage tables """

    # General message attributes
    id = models.BigAutoField(primary_key=True)
    channel_name = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        help_text="The channel the message appeared in"
    )
    forum_tags = models.JSONField(
        blank=True,
        null=True,
        help_text="The tags associated with a message.  Only applicable to forum posts."
    )
    guild_name = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        help_text="The guild the message appeared in"
    )

    # Message datetime fields
    authorship_datetime = models.DateTimeField(
        blank=True,
        null=True,
        help_text="The date/time the message was created in Discord"
    )
    author_discord_username = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        help_text="The message author's Discord id"
    )
    created_at = models.DateTimeField(
        blank=True,
        null=True,
        help_text="The date/time the message was added to the Stigsite by Stigbot"
    )
    jump_url = models.URLField(
        max_length=500,
        null=True,
        blank=True,
        help_text="A link to the message in Discord"
    )
    message_type = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        help_text="The type of message- Post, Forum or Reply"
    )
    message_thread_title = models.CharField(
        max_length=255,
        null=True,
        blank=True,
        help_text="If the message is part of a thread, this is the channel that the entire thread is in."
    )

    # Message content fields
    author = models.JSONField(
        blank=True,
        null=True,
        help_text="A JSON object containing the message author's id, bot, name, display_name, and discriminator"
    )
    reactions = models.JSONField(
        null=False,
        help_text="A list of the message's reaction data, including user, emoji, and count info"
    )

    extracted_urls_id = models.ForeignKey(
        "ExtractedUrls",
        db_column='extracted_urls_id',
        # related_name='extracted_urls',
        on_delete=models.CASCADE,
        help_text="The id of the EmbeddedMessage table entry with the message's extracted URLS"
    )

    twitter_handle = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        help_text="The message of the state written in English",
    )

    django_site_id = models.ForeignKey(
        Site,
        db_column='django_site_id',
        related_name="+",
        on_delete=models.CASCADE,
        null=True
    )
    on_site = CurrentSiteManager('django_site_id')
    objects = models.Manager()

    class Meta:
        abstract = True


class DiscordMessage(DiscordMessageBase):
    """ A table which contains information from each Discord message Stigbot picks up """

    # TODO: Replacing "content" with "original_message" would match TM and GM tables
    content = models.CharField(
        max_length=2048,
        blank=True,
        null=True
    )
    edited_at = models.DateTimeField(
        blank=True,
        null=True
    )
    # TODO: Saved_at and created_at seem similar.  Can saved_at be deleted?
    saved_at = models.DateTimeField(
        blank=True,
        null=True
    )

    class Meta:
        managed = False
        db_table = 'discord_message'


class TopAndGardenMessagesBase(DiscordMessageBase):
    """ An abstract class that contains the common fields between the TopMessage and GardenMessage tables

    This is not a database table itself
    """

    # General message attributes
    message_id = models.BigIntegerField(
        blank=True,
        null=True,
        help_text="The message's Discord ID"
    )

    # Message text fields
    editable_message = models.TextField(
        null=True,
        blank=True,
        help_text="A version of the original message for editing",
    )
    original_message = models.TextField(
        null=False,
        help_text="The message as it originally appeared in Discord",
    )

    # Message reaction fields
    # All of these fields are built from the reactions field in DiscordMessageBase
    reaction_count = models.FloatField(
        null=True,
        blank=True,
        help_text="The number of reactions a message received",
    )
    emoji_list = models.JSONField(
        null=True,
        blank=True,
        help_text="A list of all received emojis in unicode string format, including duplicate reactions"
    )
    user_count = models.IntegerField(
        null=True,
        blank=True,
        help_text="The number of users who reacted to the message, including duplicate users"
    )
    user_list = models.JSONField(
        null=True,
        blank=True,
        help_text="A list of users who reacted to the message, including duplicate users"
    )
    unique_emoji_list = models.JSONField(
        null=True,
        blank=True,
        help_text="A list of emojis in unicode string format"
    )
    unique_reaction_count = models.IntegerField(
        null=True,
        blank=True,
        help_text="The number of unique emojis a message has received",
    )
    unique_user_count = models.IntegerField(
        null=True,
        blank=True,
        help_text="The number of unique users who reacted to the message with any emoji"
    )
    unique_user_list = models.JSONField(
        null=True,
        blank=True,
        help_text="A list of unique users who reacted to the message"
    )
    counts_by_emoji = models.JSONField(
        null=True,
        blank=True,
        help_text="The number of users who reacted to the message for each emoji"
    )

    # Message datetime fields
    edited_at = models.DateTimeField(
        blank=True,
        null=True,
        help_text="The date/time the message was edited within Stigsite"
    )
    exported_at = models.DateTimeField(
        blank=True,
        null=True,
        help_text="The date/time the message was export to CSV or Airtable from Stigsite"
    )

    class Meta:
        abstract = True


class TopMessage(TopAndGardenMessagesBase):
    """ A collection of Discord messages from the DiscordMessage table have met the threshold rule.

    Each row contains one message written in one language.
    """

    added_to_garden = models.BooleanField(
        null=True,
        default=False,
        help_text="When toggled True, the message will create a GardenMessage record"
    )

    class Meta:
        managed = False
        db_table = 'top_message'

    def save(self, *args, **kwargs):
        if self.original_message != self.editable_message:
            self.edited_at = timezone.now()
        super().save(*args, **kwargs)


class GardenMessage(TopAndGardenMessagesBase):
    """ A collection of messages chosen from the TopMessage table by users through Stigsite """

    user_id = models.CharField(
        null=True,
        blank=True,
        max_length=255,
        help_text="The id of the user who triggered the TopMessage.add_to_garden"
    )

    class Meta:
        managed = False
        db_table = 'garden_message'

    def save(self, *args, **kwargs):
        if self.original_message != self.editable_message:
            self.edited_at = timezone.now()
        super().save(*args, **kwargs)


#############################################################################
# Other Discord-related models

class DiscordChannel(models.Model):

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_channel'


class DiscordGuild(models.Model):

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_guild'


class DiscordReaction(models.Model):

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    emoji = models.TextField(blank=True, null=True)
    message_id = models.BigIntegerField(blank=True, null=True)
    previous_reactions = models.JSONField(blank=True, null=True)
    previous_reaction_users = models.JSONField(blank=True, null=True)
    user = models.BigIntegerField(blank=True, null=True)
    users = models.JSONField(blank=True, null=True)
    # discord_message = models.ForeignKey(DiscordMessage, models.DO_NOTHING, db_column='discord_message', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_reaction'

    def __str__(self):
        return str(self.emoji)


class DiscordUser(models.Model):
    """ A Discord user who successfully authenticated themselves through OAuth2 """

    id = models.BigAutoField(primary_key=True)
    bot = models.BooleanField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    discriminator = models.CharField(max_length=256, blank=True, null=True)
    display_name = models.CharField(max_length=256, blank=True, null=True)
    name = models.CharField(max_length=256, blank=True, null=True)
    system = models.BooleanField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'discord_user'


class Log(models.Model):

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    json = models.JSONField(blank=True, null=True)
    log_level = models.SmallIntegerField(blank=True, null=True)
    text = models.CharField(max_length=256, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'log'


class RewardReactionLog(models.Model):
    """ A table to keep track of the reward status for messages added to the GardenMessage table """

    id = models.BigAutoField(primary_key=True)
    created_at = models.DateTimeField(blank=True, null=True)
    message_id = models.IntegerField(
        null=True,
        help_text="The id of the message to receive a reaction reward",
    )
    message_type = models.CharField(max_length=256, blank=True, null=True)
    reaction_sent = models.BooleanField(
        null=True,
        default=False,
        help_text="A record of whether a message has received the reaction reward"
    )

    class Meta:
        managed = False
        db_table = 'reward_reaction_log'


class Rule(models.Model):
    ROLE_CHOICES = [
        # CRUD admin interface for all communities (superuser)
        ('admin-user', 'Admin User'),

        # ? CRUD on community_profile view associated with user, CRUD on Rule table entries for that community, CRUD in admin interface for only that user's community,
        ('internal-user', 'Internal User'),

        # ? allowed to promote messages from TopMessage to GardenMessage and edit editable_message field
        ('gardner-user', 'Gardner User'),

        # ? ReadOnly views for GardenMessage and TopMessage views
        ('end-user', 'End User'), ]
    activated_status = models.BooleanField(
        null=True,
        default=False,
        help_text="Whether the rule is active or disabled"
    )
    channel_name = models.CharField(
        null=True,
        max_length=255,
        help_text="The channel name the rule applies to"
    )
    chosen_reactions = models.JSONField(
        null=True,
        help_text="Reactions the rule allows"
    )
    role = models.CharField(
        null=True,
        choices=ROLE_CHOICES,
        max_length=255,
        help_text="The role permissions for the rule"
    )
    rule_name = models.CharField(
        null=True,
        max_length=255,
        help_text="The name of the rule. The only rule currently accepted is 'reactions rule'."
    )
    threshold = models.FloatField(
        null=True,
        help_text="A number representing the minimum number of unique user reactions that a message must have received in order to be added to the Top Messages."
    )

    class Meta:
        managed = False
        db_table = 'stigapp_rule'


class ExtractedUrls(models.Model):
    id = models.BigAutoField(primary_key=True, help_text="The id of the messages the URLs were extracted from")
    url_list = models.JSONField(
        blank=True,
        null=True,
        help_text="A list of URLs that were in a message."
    )

    class Meta:
        managed = False
        db_table = 'extracted_urls'

    def __str__(self):
        return str(self.id)
