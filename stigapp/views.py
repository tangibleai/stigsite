import csv
import config
import datetime
import django_filters
import json
import pytz

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template.defaultfilters import register
from django.urls import reverse
from django.utils import timezone
from django.views.generic import ListView
from .models import TopMessage
from stigapp.airtable_integration_supabase import AirTableServiceSupabase
from scripts.create_leaderboard_data import *
from stigapp.models import (
    DiscordMessage,
    DiscordReaction,
    GardenMessage
)
from django.contrib.sites.shortcuts import get_current_site


@register.filter(name='dict_key')
def dict_key(d, k):
    return d[k]


def home(request):
    return render(request, 'stigapp/home.html')


def terms(request):
    return render(request, 'stigapp/terms-of-service.html')


def privacy(request):
    return render(request, 'stigapp/privacy-policy.html')


def about(request):
    return render(request, 'stigapp/about.html')


def page_not_found_404_view(request, exception):
    return render(request, 'stigapp/404.html', status=404)


@login_required(login_url='/login')
def update_top_message_list(request):

    up = manage_leaderboard_data()
    up.generate_leaderboard_data()

    return HttpResponseRedirect(reverse('top_messages'))


def extract_reaction_data(entry):
    """ Converts reaction and user data from JSON to strings for the TopMessage and Garden Message CSV export functions

    Input:
    GardenMessage object (29)

    Output:
    {
        'emoji_list': '😭, 🥵, 🪂, 🛤️, 😭, 🥵, 🪂, 🛤️',
        'unique_emoji_list': '🛤️, 🪂, 😭, 🥵',
        'user_list': 'thompsgj8407, thompsgj8407, thompsgj8407, thompsgj8407, gregt3302, gregt3302, gregt3302, gregt3302', 'unique_user_list': 'thompsgj8407, gregt3302'
    }

    """

    emoji_list = []
    user_list = []

    for reaction in entry.reactions:
        emoji_list.append(reaction['emoji'])
        user_list.append(f"{reaction['user']['name']}{reaction['user']['discriminator']}")

    reaction_data_dict = {
        "emoji_list": ", ".join(emoji_list),
        "unique_emoji_list": ", ".join(list(set(emoji_list))),
        "user_list": ", ".join(user_list),
        "unique_user_list": ", ".join(list(set(user_list))),
        "counts_by_emoji": "\n".join("{}: {}".format(k, v) for k, v in entry.counts_by_emoji.items())
    }

    return reaction_data_dict


# class GardenMessageFilter(django_filters.FilterSet):
#     unique_user_count = django_filters.NumberFilter()
#     unique_user_count__gt = django_filters.NumberFilter(field_name='unique_user_count', lookup_expr='gt')
#     unique_user_count__lt = django_filters.NumberFilter(field_name='unique_user_count', lookup_expr='lt')

#     class Meta:
#         model = GardenMessage
#         fields = ['unique_user_count']


# class GardenMessageListView(ListView):
#     """ A view that uses gardenmessage_list.html to display an html table for both GardenMessage and TopMessage Supabase tables

#     Accesed in the template with gardenmessage_list"""
#     model = GardenMessage
#     ordering = ['-reaction_count']
#     login_url = '/login'

#     def get_queryset(self):
#         queryset = GardenMessage.objects.all()

#         return GardenMessageFilter(self.request.GET, queryset=queryset)

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['filter'] = GardenMessageFilter(self.request.GET, queryset=self.get_queryset())
#         return context


class GardenMessageListView(ListView):
    """ A view that uses gardenmessage_list.html to display an html table for both GardenMessage and TopMessage Supabase tables

    Accesed in the template with gardenmessage_list"""
    model = GardenMessage
    ordering = ['-reaction_count']
    login_url = '/login'

    # f = GardenMessageFilter(request.GET, queryset=GardenMessage.objects.all())

    # def get_context_data(self, **kwargs):
    #     """ Puts the TopMessage table into a context variable because this ListView only contains GardenMessage by default

    #     Accessed in the template with topmessage_list"""
    #     context = super(GardenMessageListView, self).get_context_data(**kwargs)
    #     context['topmessage_list'] = TopMessage.objects.all().order_by('-reaction_count')
    #     return context


class TopMessageListView(LoginRequiredMixin, ListView):
    """ A view that uses topmessage_list.html to display an html table for the TopMessage Supabase table

    Accesed in the template with gardenmessage_list"""
    model = TopMessage
    ordering = ['-reaction_count']
    login_url = '/login'


class DiscordMessageListView(ListView):
    model = DiscordMessage


class DiscordReactionListView(ListView):
    model = DiscordReaction
    ordering = ['-previous_reactions']


def retrieve_csv_column_headings(table):
    """ Returns the headings for the CSV export """
    if table == 'garden':
        headings = garden_message_column_headers = [
            'Author', 'Twitter Handle',
            'Message ID', 'Message Type', 'Original Message', 'Editable Message',
            'All Reactions', 'Unique Reactions',
            'All Emojis', 'Unique Emojis', 'Totals by Emoji',
            'Total Users', 'Unique Users',
            'Total User List', 'Unique User List',
            'Created', 'Edited',
            'URL'
        ]
    else:
        headings = top_message_column_headers = [
            'Author', 'Message ID', 'Message Type', 'Message',
            'All Reactions', 'Unique Reactions',
            'All Emojis', 'Unique Emojis', 'Totals by Emoji',
            'Total Users', 'Unique Users',
            'Total User List', 'Unique User List',
            'Created',
            'Added to Garden', "URL"
        ]
    return headings


def get_export_datetimetz():
    tz = pytz.timezone(config.TIME_ZONE)
    time_info = tz.localize(datetime.datetime.now())
    return time_info


def make_csv_row_data_list(entry, table, test):
    """ Build a list of information about a message

    Fields should be in the same order as the retrieve_csv_column_headings
    """
    if test == 'false':
        if entry.exported_at:
            exported_at = entry.exported_at
        else:
            exported_at = get_export_datetimetz()
    else:
        exported_at = ''

    reaction_data_dict = extract_reaction_data(entry)

    if table == 'garden':
        message_data = [
            entry.author_discord_username,
            entry.twitter_handle,

            entry.message_id,
            entry.message_type,
            entry.original_message,
            entry.editable_message,

            entry.reaction_count,
            entry.unique_reaction_count,

            reaction_data_dict['emoji_list'],
            reaction_data_dict['unique_emoji_list'],

            reaction_data_dict['counts_by_emoji'],

            entry.user_count,
            entry.unique_user_count,

            reaction_data_dict['user_list'],
            reaction_data_dict['unique_user_list'],

            entry.created_at,
            entry.edited_at,

            entry.jump_url,

            exported_at,
        ]
    else:
        message_data = [
            entry.author_discord_username,
            entry.message_id,
            entry.message_type,
            entry.original_message,

            entry.reaction_count,
            entry.unique_reaction_count,

            reaction_data_dict['emoji_list'],
            reaction_data_dict['unique_emoji_list'],

            reaction_data_dict['counts_by_emoji'],

            entry.user_count,
            entry.unique_user_count,

            reaction_data_dict['user_list'],
            reaction_data_dict['unique_user_list'],

            entry.created_at,

            entry.added_to_garden,
            entry.jump_url,

            exported_at,
        ]
    return message_data


@login_required(login_url='/login')
def export_csv(request, table, selected, test):
    """ Creates a CSV file for the selected message table

    Parameters:
    table - The database table ('garden' or 'top')
    selected - How many messages are sent ('all' or 'unadded')
    test - Whether the export is a test or for real updates
     """

    # Set up the file response
    filename = f"{table.capitalize()}Message-{datetime.date.today()}.csv"
    response = HttpResponse(
        content_type='text/csv',
        headers={'Content-Disposition': 'attachment; filename=' + filename}
    )
    response.write(u'\ufeff'.encode('utf8'))
    writer = csv.writer(response)

    # Build the CSV headings
    headings = retrieve_csv_column_headings(table)

    if test == 'false':
        headings.append("Exported")
        export_time = get_export_datetimetz()
    else:
        export_time = ''

    # Write headings to first row
    writer.writerow(headings)

    # Get the data from the GardenMessage or TopMessage table
    if table == 'garden':
        if selected == 'unexported':
            message_table = GardenMessage.on_site.filter(exported_at__isnull=True)
        else:
            message_table = GardenMessage.on_site.all()
    else:
        if selected == 'unexported':
            message_table = TopMessage.on_site.filter(exported_at__isnull=True)
        else:
            message_table = TopMessage.on_site.all()

    for message in message_table:
        message_data = make_csv_row_data_list(message, table, test)

        writer.writerow(message_data)

        # Check for updating export data in message table
        if test == 'false':
            if table == 'garden':
                GardenMessage.on_site.filter(
                    message_id=message.message_id
                ).update(
                    exported_at=export_time
                )
            else:
                TopMessage.on_site.filter(
                    message_id=message.message_id
                ).update(
                    exported_at=export_time
                )

    return response


def export_to_airtable(request):
    inst = AirTableServiceSupabase()
    inst.bulk_add_leaderboard()

    return HttpResponseRedirect(reverse('home'))
