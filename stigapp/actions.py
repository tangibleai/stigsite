""" Parameterized admin-defined functions  (ADFs instead of UDFs) that are configurable within a django app or database like stigsite

actions that are the names of coroutines for messages will automatically be run within the on_message() coroutine
"""
# actions.py
import datetime
from loggers import log_debug, log_error
from utils import isoformat


def record_message(message=None):
    log_debug(f"Attempting to record message: {message}\n")
    if message is None:
        return log_error(text='Empty message_dict found in record_message()')
    message_dict = dict(
        id=message.id,  # record_id is printed on reaction.message? rather than id?
        created_at=isoformat(message.created_at),
        saved_at=isoformat(),
        edited_at=isoformat(message.edited_at),
        content=str(message.content),
        author=to_dict(message.author, AUTHOR_ATTRS),  # , indent=json_indent),
        reactions=to_listodicts(message.reactions, REACTION_ATTRS),  # , indent=json_indent)
    )
    return update_or_create(table_name='discord_message', record_dict=message_dict)


def record_user(user=None):
    log_debug(f"Attempting to record user: {user}\n")
    user_dict = to_dict(user, USER_ATTRS)
    log_warning(f"Attempting to record user_dict: {user_dict}\n")
    if user is None:
        return log_error(text='Empty user obj found in record_user()')
    return update_or_create(table_name='discord_user', record_dict=user_dict)


def record_reaction(reaction=None, user=None):
    """ record an individual reaction as well as the total reactions dict for all messages"""
    log_debug(f"Attempting to record reaction:\n{reaction}\nfor user:\n{user}\n")
    reaction_dict = {}
    # TODO: instead of reaction.message.reactions, use our own message.previous_reactions
    #       Our supabase message.previous reactions listofdicts contains the user for each reaction instead of just count

    if reaction is None:
        return log(text='Empty reaction or user found in record_reaction()')
    reaction_dict = to_dict(reaction, REACTION_ATTRS)
    reaction_dict['user'] = user.id

    previous_reactions = to_listodicts(reaction.message.reactions, REACTION_ATTRS, REACTION_COROS)
    previous_reactions = [r for r in previous_reactions]
    # users = list(await reaction.users().flatten())
    # print(f'\nreaction: {reaction}\nusers: {users}')
    # previous_reaction_users=SUPA.table('message').select('reaction_users')
    db_record = dict(
        message_id=reaction.message.id,
        user=user.id if user is not None else None,
        emoji=(str(reaction.emoji) or str(reaction)),
        # users=users,
    )

    previous_reactions.append(reaction_dict)
    db_record['previous_reactions'] = previous_reactions
    # reaction.message.previous_reactions = previous_reactions
    if reaction.message:
        record_message(reaction.message)
        return update_or_create(table_name='discord_reaction', record_dict=reaction_dict)
    return log(text=f'Empty reaction.message in record_reaction({reaction_dict})')
