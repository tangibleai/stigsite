"""

1. visit the discord developer dashboard and generate a URL for a bot with admin privileges (the permissions bit mask integer in the URL should be long).

2. start the discord bot service:

>>> python log_discord_messages.py
{...}
We have logged in as daotest#7533

3. Visit the discord server you generated the URL for and you should see your bot among the list of onine users.
4. In the online users list on discord right click the bot and invite them to the server.
5. In discord say "hello what's up?", lowercase hello is required, (on any public channel)?

You should see something like this logged in the terminal where the bot is running.
This is the message the bot posted on the server (because that's the only print statement in the on_message fun)
This is **NOT** the original message from the user:

Received message from bot (daotest#7533):
    Hello! You just said: hello what's up?
message_obj:
<Message
    id=1009925779129389138
    channel=<TextChannel
        id=992807246788567134
        name='general'
        position=4
        nsfw=False
        category_id=992807246788567133
        news=False>
    type=<MessageType.default: 0>
    author=<Member
        id=1007105836092497950
        name='daotest'
        discriminator='7533'
        bot=True
        nick=None
        guild=<Guild
            id=992807246113276064
            name='qary'
            shard_id=0
            chunked=False
            member_count=7
            >
        >
    flags=<MessageFlags value=0>
>


"""


import config
import datetime
import discord
import hashlib
import os
import re
import time
import json

from discord import Emoji, TextChannel, Thread, DMChannel
from discord.ext import commands
from supabase import create_client

from coro_utils import get_coroutine
from discord_schema import AUTHOR_ATTRS, REACTION_ATTRS, USER_ATTRS  # , REACTION_COROS
from loggers import log, log_error, log_debug, log_warning
from stigapp.airtable_integration_supabase import AirTableServiceSupabase

from utils import isoformat, to_dict, to_listodicts


if not os.environ.get('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'stigsite.settings'
import django
django.setup()

from stigapp.models import DiscordMessage
from stigapp.models import TopMessage
from stigapp.models import GardenMessage


SUPA = create_client(config.SUPABASE_URL, config.SUPABASE_KEY)

intents = discord.Intents.default()
intents.message_content = True


discord_message_table_fields = " ".join([f.name for f in DiscordMessage._meta.get_fields()])
top_message_table_fields = " ".join([f.name for f in TopMessage._meta.get_fields()])
garden_message_table_fields = " ".join([f.name for f in GardenMessage._meta.get_fields()])


# TODO: Create Django model class Action
#    Action.trigger_type = choices field ['message', 'reaction']
#    Action.func_name = varchar max_len 64
#    Action.func_args = jsonfield
#    Action.func_kwargs = jsonfield
#    Action.enabled = bool
DEFAULT_MESSAGE_ACTIONS = (
    # TODO: change these default args to None, and then allow the 3-tuple to be a 1-tuple and then just a str (without messing it up)
    ('record_message', (), {}),
    # ('add_reaction', ('✔',), dict(condition=dict(regex=f'\\b{BOTNAME}\\b'))),
    # ('add_reaction', ('✍️',), {}),  # 🛢  💾  📓  📄  📒  🏷   ✔  ✅  ✴  ✳  🔹  ◼  ▫
    # ('reply', ['noted'], {}),
    # TODO allow format string templates with `def render_fstring(fstring, message):`
    # ('channel.send', ['noted'], {}),
)

DEFAULT_REACTION_ACTIONS = (
    ('record_reaction', (), {}),
    # ('add_reaction', ['✔'], {}),  # ✍️  🛢  💾  📓  📄  📒  🏷   ✔  ✅  ✴  ✳  🔹  ◼  ▫
    # ('reply', ['noted'], {}),
    # TODO allow format string templates with `def render_fstring(fstring, message):`
    # ('channel.send', ['noted'], {}),
)

# Python function names that user's can specify as actions when new messages, reactions, or users are received from discord
ALLOWED_FUNS = 'record_reaction record_message insert_supabase add_reaction reply channel.send'.split()

# client = discord.Client(intents=intents)
client = commands.Bot(command_prefix=".", intents=intents)


@client.event
async def on_ready():
    print(f'Bot named {client.user} is listening for discord messages...')


def get_fun_args_kwargs(fun_name, *args, **kwargs):
    if fun_name in ALLOWED_FUNS:
        # TODO: Probably not thread safe to make ALLOWED_FUNS a subset of the dict globals()
        fun = globals().get(fun_name)
    else:
        fun = log_error
        fun_name = ''.join([c for i, c in enumerate(str(fun_name)) if i < 256])
        err_message = f"ERROR (SECURITY): {fun_name} is not in log_discord_messages.ALLOWED_FUNS!!!"
        args = [err_message] + list(args)
    return fun, args, kwargs


def update_or_create(table_name='discord_message', record_dict=None):
    log_debug(f"Attempting update_or_create(record_dict={record_dict})")
    assert record_dict is not None
    record_id = record_dict.pop('id', None)
    if record_id is not None:
        log_debug(f"Updating exisitng DB for {table_name}.id: {record_id}")
        resp = SUPA.table(table_name).select("*").eq('id', record_id).execute()
        if len(resp.data):
            return SUPA.table(table_name).update(record_dict).eq("id", record_id).execute()
        record_dict['id'] = record_id
    log_debug(f"Creating new record for {table_name}: {record_dict}!")

    return SUPA.table(table_name).insert(record_dict).execute()


def extract_urls_from_message(message):
    """ Parses a Discord message for any urls inside, saves them to extracted_urls, and returns the ForeignKey id

    Input: Discord Message object
    Output: ForeignKey Id of extracted_urls
    """

    urls = re.findall('(?:(?:https?|ftp):\/\/)?[\w/\-?=%.]+\.[\w/\-&?=%.]+[^. ]', message.content)

    if urls == []:
        return None
    else:
        urls_query_result = SUPA.table('extracted_urls').select('*').eq("id", message.id).execute()
        existing_urls = urls_query_result.data

        if existing_urls == []:

            # Need to update this to save the URLs to Supabase and return a foreign key
            data = {
                "id": message.id,
                "url_list": urls
            }

            url_entry = SUPA.table('extracted_urls').insert(data).execute()
            return url_entry.data[0]['id']
        else:
            return existing_urls[0]['id']


def record_message(message=None, twitter_handle=None):
    log_debug(f"Attempting to record message: {message}\n")
    if message is None:
        return log_error(text='Empty message_dict found in record_message()')

    forum_tags = []
    message_type, message_channel, message_thread_title = get_message_type_and_channel(message)

    extracted_urls = extract_urls_from_message(message)

    message_dict = dict(
        id=message.id,  # record_id is printed on reaction.message? rather than id?
        created_at=isoformat(message.created_at),
        saved_at=isoformat(datetime.datetime.now()),
        authorship_datetime=isoformat(message.created_at),
        channel_name=message_channel,
        message_thread_title=message_thread_title,
        guild_name=message.guild.name,
        # TODO: Throws object of type json is not serializable
        # edited_at=isoformat(message.edited_at),
        content=str(message.content),
        jump_url=str(message.jump_url),
        author=to_dict(message.author, AUTHOR_ATTRS),  # , indent=json_indent),
        author_discord_username=str(message.author),
        reactions=to_listodicts(message.reactions, REACTION_ATTRS),  # , indent=json_indent)
        forum_tags=forum_tags,
        message_type=message_type,
        extracted_urls_id=extracted_urls,
        # urls=extracted_urls
        twitter_handle=twitter_handle
    )
    return update_or_create(table_name='discord_message', record_dict=message_dict)


def record_user(user=None):
    log_debug(f"Attempting to record user: {user}\n")
    user_dict = to_dict(user, USER_ATTRS)
    log_warning(f"Attempting to record user_dict: {user_dict}\n")
    if user is None:
        return log_error(text='Empty user obj found in record_user()')
    return update_or_create(table_name='discord_user', record_dict=user_dict)


def get_message_type_and_channel(message):
    """ Evaluates the message.channel to see whether a message is a Message, Reply, or Forum and sets the appropriate channel information

    Input: Discord Message object
    Output:
    - message_type: Message, Forum, or Reply
    - message_channel: The channel the message or thread is in
    - message_thread_title: If the message is in a thread (Forum or Reply), the title is the first message's text (Message) or title (Forum)
    """
    message_type = ''
    if isinstance(message.channel, TextChannel):
        message_type = "Message"
        message_channel = message.channel.name
        message_thread_title = ''
    elif isinstance(message.channel, Thread):
        """ Forum post is starting message of the thread.

        Reply is in response to TextChannel, which starts the Thread
        """
        if message.channel.starting_message == message:
            message_type = "Forum"
            message_channel = message.channel.parent.name
            message_thread_title = message.channel.name
        else:
            message_type = "Reply"
            message_channel = message.channel.parent.name
            message_thread_title = message.channel.name
    else:
        log_debug(f"Adding an entry to DiscordMessage table: {message}")

    return message_type, message_channel, message_thread_title


def record_reaction(message=None, reaction=None, all_reactions=None, user=None):
    """ Record an individual reaction as well as the total reactions dict for all messages"""
    log_debug(f"Attempting to record reaction:\n{reaction}\nfor user:\n{user}\n")

    # Checks for empty reaction
    reaction_dict = {}
    if reaction is None:
        return log(text='Empty reaction or user found in record_reaction()')

    # Evaluates whether there is an existing Supabase entry to build the message_dict object
    existing_message_data = SUPA.table('discord_message').select("*").eq('id', reaction.message.id).execute()
    if existing_message_data.data != []:
        new_fields = build_field_backwards_compatibility_object(message)

        # If there is data, update it using existing data
        message_dict = dict(
            id=existing_message_data.data[0]['id'],  # good
            created_at=existing_message_data.data[0]['created_at'],
            saved_at=existing_message_data.data[0]['saved_at'],
            content=existing_message_data.data[0]['content'],
            author=existing_message_data.data[0]['author'],
            reactions=all_reactions
        )

        message_dict.update(new_fields)
    else:
        forum_tags = []

        message_type, message_channel, message_thread_title = get_message_type_and_channel(message)

        message_dict = dict(
            id=message.id,
            created_at=isoformat(message.created_at),
            saved_at=isoformat(datetime.datetime.now()),
            content=str(message.content),
            author={
                'id': message.author.id,
                'bot': message.author.bot,
                'name': message.author.name,
                'display_name': message.author.display_name,
                'discriminator': message.author.discriminator
            },
            reactions=all_reactions,
            jump_url=str(message.jump_url),
            forum_tags=forum_tags,
            message_type=message_type,
            author_discord_username=f"{message.author.name}#{message.author.discriminator}",
            authorship_datetime=isoformat(message.created_at),
            channel_name=message_channel,
            message_thread_title=message_thread_title,
            guild_name=message.guild.name,
            # LALALA
        )

    # Use the message data to update/create a message entry in DiscordMessage
    update_or_create(table_name='discord_message', record_dict=message_dict)

    # 4. Build db_record for DiscordReaction
    db_record = dict(
        message_id=reaction.message.id,
        user=user.id if user is not None else None,
        emoji=(str(reaction.emoji) or str(reaction)),
        # users=users,
        previous_reactions=all_reactions
    )

    # Uses the data to update DiscordReaction
    return update_or_create(table_name='discord_reaction', record_dict=db_record)


def create_list_data(reaction_data, data_type):
    """ Returns an array of users or emoji from the reaction field """
    content_list = []
    for reaction in reaction_data:
        try:
            content_list.append(reaction[data_type])
        except KeyError:
            pass
    return content_list


def make_reaction_data_object(reactions):
    """ Iterates over a list of reactions to find the user and emoji data  """

    user_list = create_list_data(reactions, 'user')
    unique_user_list = list({v['id']: v for v in user_list}.values())

    # Get a list of emojis for a message
    emoji_list = create_list_data(reactions, 'emoji')
    unique_emoji_list = list(set(emoji_list))

    # Get total count by reaction (emoji)
    counts_by_emoji_dict = {}
    for reaction in reactions:
        counts_by_emoji_dict[reaction['emoji']] = len(
            [i for i in reactions if i['emoji'] == reaction['emoji']])

    reaction_data = {
        'user_list': user_list,
        'user_count': len(user_list),
        'unique_user_list': unique_user_list,
        'unique_user_count': len(unique_user_list),

        'emoji_list': emoji_list,
        'reaction_count': len(emoji_list),
        'unique_emoji_list': unique_emoji_list,
        'unique_reaction_count': len(unique_emoji_list),
        'counts_by_emoji': counts_by_emoji_dict,
    }
    return reaction_data


def build_top_message_object(discord_message):
    """ Builds the data object to make a new TopMessage entry """

    message_data = {
        'author': discord_message['author'],
        'message_id': discord_message['id'],
        'created_at': discord_message['created_at'],
        'original_message': discord_message['content'],
        'editable_message': discord_message['content'],
        'reactions': discord_message['reactions'],
        'jump_url': discord_message['jump_url'],
        'forum_tags': discord_message['forum_tags'],
        'message_type': discord_message['message_type'],

        'author_discord_username': f"{discord_message['author']['name']}#{discord_message['author']['discriminator']}",
        'authorship_datetime': discord_message['authorship_datetime'],
        'channel_name': discord_message['channel_name'],
        'guild_name': discord_message['guild_name'],
        'added_to_garden': False,
        'extracted_urls_id': discord_message['extracted_urls_id']
    }

    reaction_data = make_reaction_data_object(discord_message['reactions'])
    message_data.update(reaction_data)
    return message_data


# Asynchronous functions===========================================

async def get_reaction_author(reaction):
    """ Calls the Pycord .users() function to get the user info for a Reaction object """
    users = set()
    async for user in reaction.users():
        users.add(user)

    return users


async def build_reaction_history(message):
    """ Retrieves all the reactions associated with a message """

    all_reactions_filtered = []
    for reaction in message.reactions:
        user_result = await get_reaction_author(reaction)

        for user in user_result:
            # NOTE: Count needs to be aggregated manually if set here, otherwise each similar emoji has the total count of similar emojis
            # TODO: 'me' is always false.  May need to be adjusted
            reaction_dict = {
                'user': {
                    'id': user.id,
                    'name': user.name,
                    'discriminator': user.discriminator
                },
                'count': 1,
                'me': reaction.me,
                'message__id': reaction.message.id
            }
            if isinstance(reaction.emoji, Emoji):
                reaction_dict['emoji'] = reaction.emoji.name
            else:
                reaction_dict['emoji'] = reaction.emoji

            all_reactions_filtered.append(reaction_dict)
    return all_reactions_filtered


async def check_top_message_eligibility_old(reaction_data, backwards_compatibility_object=None):
    """ Checks whether a message in the DiscordMessage table has passed the unique user reaction threshold to be included in TopMessage

    Uses 'reactions rule' in the rule database table if available.
    If not, the treshold is the number of reactions from 5 unique users
    """

    # TODO: Probably need to split checking and processing based on results into two separate functions.  check_top_message_eligibility should return true/false.  If true, add if not added.  If false, skip

    user_list = []
    reactions = reaction_data[0].get('previous_reactions', reaction_data)

    # NOTE/TODO: If only one reaction, then there is no reaction_data[0]['previous_reactions'], so it skips the for loop below (0 threshold will not be awarded with a post, even if 1 user reacts)

    # NOTE: Changed this
    # for reaction in reaction_data[0]['previous_reactions']:
    for reaction in reactions:
        try:
            user_list.append(reaction['user']['name'])
        except KeyError:
            pass

    unique_user_list = list(set(user_list))

    reaction_threshold = await get_reaction_threshold_value()

    if len(unique_user_list) >= reaction_threshold:
        top_message = SUPA.table('top_message').select("*").eq('message_id', reaction_data[0]['message_id']).execute()

        if top_message.data == []:
            discord_message = SUPA.table('discord_message').select("*").eq('id', reaction_data[0]['message_id']).execute()
            message_data = build_top_message_object(discord_message.data[0])
            return SUPA.table('top_message').insert(message_data).execute()
        else:
            current_reactions = reaction_data[0]['previous_reactions']
            updated_reaction_data = make_reaction_data_object(current_reactions)

            updated_reaction_data.update(backwards_compatibility_object)

            editable_message = top_message.data[0]['editable_message'] if top_message.data[0][
                'editable_message'] != '' and top_message.data[0]['editable_message'] != None else top_message.data[0]['original_message']
            updated_reaction_data['editable_message'] = editable_message
            updated_reaction_data['edited_at'] = top_message.data[0]['edited_at']

            return SUPA.table('top_message').update(updated_reaction_data).eq("message_id", reaction_data[0]['message_id']).execute()
    else:
        return False


# Custom Pycord command =================================
@ client.command()
async def add_reaction_to_top_message(message_id, message_type):
    """ Adds a flower emoji to TopMessage entries that are added to the GardenMessage table

    Forums and Replies are essentially threads for the purpose of searching and adding a reply.  Only Messages are treated differently

    >>> await add_reaction_to_top_message(message_id, message_type)
    """
    # Thread message
    reaction = '🌺'

    for guild in client.guilds:
        # TODO: Need to simplify this, probably by adding data to discord_message table
        if message_type == 'Message':
            for text_channel in guild.text_channels:
                try:
                    message = await text_channel.fetch_message(message_id)

                    SUPA.table('reward_reaction_log').update({'reaction_sent': True}).eq("message_id", message_id).execute()

                    return await message.add_reaction(reaction)
                except Exception as e:
                    print(e)
        elif message_type == 'Reply' or message_type == 'Forum':
            for thread in guild.threads:
                try:
                    message = await thread.fetch_message(message_id)

                    SUPA.table('reward_reaction_log').update({'reaction_sent': True}).eq("message_id", message_id).execute()

                    return await message.add_reaction(reaction)
                except Exception as e:
                    print(e)
        else:
            return log(text=f'Message type of {message_type} not handled in add_reaction_to_top_message')


def build_field_backwards_compatibility_object(message):
    additional_fields = {
        "authorship_datetime": isoformat(message.created_at),
        "channel_name": message.channel.name,
        "guild_name": message.guild.name,
        # "editable_message": a,
        # "edited_at": a
    }
    return additional_fields

# Pycord event functions =====================================


async def check_feature_restriction(message):
    """ A whitelist of approved users for whom a feature is available while testing """
    approved_admin_users = [
        # 'insert discord_tags',
        'thompsgj#8407',
        'gregt#3302',
        # 'hobs#3386',
        # 'hobs#9238',
        # 'Sha#6179',
        # 'ronent#2267',
        # 'mariadyshel#6267',
        # 'richa#0775',
        # 'GideonRo#3175',
        # 'Irem#3362',
        # 'enti#1546',
        # 'bear100#9085',
        # 'acidlazzer#5796',
        'rochdikhalid#7903',
    ]
    author_discord_username = f"{message.author.name}#{message.author.discriminator}"
    return author_discord_username in approved_admin_users


async def create_user_password():
    """ Creates a hash to be used for password creation for a new user account """

    hash = hashlib.sha1()
    hash.update(str(time.time()).encode('utf-8'))
    return hash.hexdigest()[:7]

# A temporary object to hold actions (key) and associated messages (values)
bot_prompts = {
    'request_twitter_handle': "Thanks for your message!  To help us curate our channel's content, we'd love to be able to credit you on other platforms if we repost your message.  Please respond to this message with your Twitter handle without the '@'.",
    'request_twitter_handle_complete': "Thanks for your response!  I've added your Twitter handle to your profile in Stigsite.  You can update it there if you ever need to."
}


async def receive_twitter_username_through_dm(message):
    """ Listens for a DMchannel (direct message) from a user in response to a twitter_handle request.

    Updates a user account in Stigsite with the twitter_handle """
    if isinstance(message.channel, DMChannel):
        # NOTE / TODO: Assumed this message should be recorded.  Need to refine the logic here.

        user_query_result = SUPA.table('auth_user').select("*").eq('id', message.author.id).execute()

        if user_query_result.data[0]['twitter_handle'] == '':
            SUPA.table('auth_user').update({'twitter_handle': message.content}).eq('id', message.author.id).execute()
            dm_channel = await client.create_dm(message.author)

            # Acknowledgement message
            await dm_channel.send(content=bot_prompts['request_twitter_handle_complete'])

            # Update databases
            author_discord_username = f"{message.author.name}#{message.author.discriminator}"

            SUPA.table('discord_message').update({'twitter_handle': message.content}).eq(
                'author_discord_username', author_discord_username).execute()
            SUPA.table('top_message').update({'twitter_handle': message.content}).eq(
                'author_discord_username', author_discord_username).execute()
            SUPA.table('garden_message').update({'twitter_handle': message.content}).eq(
                'author_discord_username', author_discord_username).execute()
        return True
    else:
        return False


async def get_twitter_handle(message, operation='reaction', community_settings=None):
    """ Checks a Stigsite account for a twitter_handle.  If no Twitter handle is associated with a user, the bot prompts the user to supply one through a direct message.

    """

    """
    TODOs:
    - There needs to be a mechanism to stop the twitter_handle from updating with every DM. (DONE)
    - Need to check that the DM is in response to an initial DM from the bot (not just a random person DMing the bot).
    - Need to provide an update option in case the user mistypes.
    - Fix the password to match the existing style (HALF IMPLEMENTED - need to finish replicating Django's password generation procedure)
    -Separate the logic from on_message into it's own function (DONE)
    - Respond with an acnknowledgement that the twitter_handle has been received and saved.  This could be a good opportunity to tell them how to update it. (DONE - not the update message)
    - Should the bot say they made an account for them at Stigsite?

    """

    # Handle the initial check and messaging for Twitter handle request
    user_query_result = SUPA.table('auth_user').select("*").eq('id', message.author.id).execute()

    # If there is no user profile in Stigsite
    twitter_handle = ''
    password = await create_user_password()

    if community_settings == None or community_settings.get('enable_twitter_handle_request', False) is False:
        return twitter_handle

    if user_query_result.data == []:
        # on_raw_reaction_add/remove should not send a request to the user
        if operation == 'reaction':
            return twitter_handle

        user = {
            'id': message.author.id,
            'password': password,
            'discord_tag': f"{message.author.name}#{message.author.discriminator}",
            'username': message.author.name,
            # 'first_name': message.author.nick,
            'twitter_handle': twitter_handle,
            'requested_twitter_handle': True
        }
        SUPA.table('auth_user').insert(user).execute()

        dm_channel = await client.create_dm(message.author)
        await dm_channel.send(content=bot_prompts['request_twitter_handle'])
    else:
        if user_query_result.data[0]['twitter_handle'] == '' and (user_query_result.data[0]['requested_twitter_handle'] is False or user_query_result.data[0]['requested_twitter_handle'] is None):
            dm_channel = await client.create_dm(message.author)
            await dm_channel.send(content=bot_prompts['request_twitter_handle'])
            SUPA.table('auth_user').update({'requested_twitter_handle': True}).eq('id', message.author.id).execute()
        else:
            twitter_handle = user_query_result.data[0]['twitter_handle']
    return twitter_handle


async def look_up_message(message_id):
    # Forum Message id
    # message_id = 1045303710705778728

    # Forum Reply Message id
    # message_id = 1045307909665464330

    for guild in client.guilds:
        for text_channel in guild.text_channels:
            try:
                message = await text_channel.fetch_message(message_id)
                # channel_name = message.channel
                # guild_name = message.guild
                return message
            except:
                pass
        for thread in guild.threads:
            try:
                message = await thread.fetch_message(message_id)
                return message
            except:
                pass
        for forum_channel in guild.forum_channels:
            for thread in forum_channel.threads:
                try:
                    message = await thread.fetch_message(message_id)
                    return message
                except:
                    pass
            try:
                message = forum_channel.get_partial_message(message.id)
                return message
            except:
                pass
    # return {'channel_name': '', 'guild_name': ''}
    return "No message"


async def get_site_id(guild_name):
    if guild_name == "thompsgj's server":
        site_id = 1
    elif guild_name == 'Common Sense [makers]':
        site_id = 3
    elif guild_name == 'Token Engineering Commons':
        site_id = 2
    elif guild_name == 'qary':
        site_id = 4
    else:
        site_id = 1
    return site_id


async def fill_missing_channel_and_guild_data():
    SUPA = create_client(config.SUPABASE_URL, config.SUPABASE_KEY)
    supabase_results = SUPA.table('discord_message').select('*').execute()
    supabase_data = supabase_results.data

    for supabase_entry in supabase_data:
        if supabase_entry['django_site_id'] and supabase_entry['guild_name']:
            continue

        print(f"Updating database entry: {supabase_entry['id']}")
        if supabase_entry['guild_name']:
            continue
            site_id = await get_site_id(supabase_entry['guild_name'])
            update_data_object = {'django_site_id': site_id}
        else:
            discord_data = await look_up_message(supabase_entry['id'])

            if discord_data == "No message":
                continue

            if isinstance(discord_data.channel, TextChannel):
                channel_name = discord_data.channel.name
            elif isinstance(discord_data.channel, Thread):
                if discord_data.channel.starting_message == supabase_entry['content']:
                    channel_name = discord_data.channel.parent.name
                else:
                    channel_name = discord_data.channel.parent.name

            site_id = await get_site_id(discord_data.guild.name)
            update_data_object = {
                'channel_name': channel_name,
                'guild_name': discord_data.guild.name,
                'django_site_id': site_id
            }
        # return False

        # if site_id > 0:
        SUPA.table('discord_message').update(update_data_object).eq('id', supabase_entry['id']).execute()


async def get_community_settings(message):
    results = SUPA.table('community_profile').select("*").execute()
    results = results.data
    community_settings = {}
    for community in results:
        try:
            guild_dic = community['monitored_guilds'].values()
        except:
            guild_dic = []

        # Tests if the message's guild name is in the list of values
        if message.guild.name in guild_dic:
            community_settings['enable_twitter_handle_request'] = community['enable_twitter_handle_request']
            break
        else:
            community_settings['enable_twitter_handle_request'] = False

    return community_settings


@ client.event
async def on_message(message, message_actions=DEFAULT_MESSAGE_ACTIONS):  # , json_indent=2):
    print(f'Received message from {message.author}.')
    print(f'Received message {message}.')

    # don't accidentally reply to your own messages (infinite recursion)!!!
    if message.author == client.user:
        return

    if message.content:
        # admin_user = await check_feature_restriction(message)
        # if admin_user is True and community_settings['enabled_twitter_request'] is True:
        received_dm = await receive_twitter_username_through_dm(message)
        if received_dm is True:
            return True

        community_settings = await get_community_settings(message)
        general_message = await build_general_message_object(message, 'message', community_settings)
        discord_message_data = await build_message_object_by_table(general_message, 'discord_message')
        SUPA.table('discord_message').insert(discord_message_data).execute()

        # Only used to fill guild_name, channel_name, and site_id for old data
        # if message.content == "REBUILD":
        #     return await fill_missing_channel_and_guild_data()

        #
        #
        #
        # OLD WAY OF RECORDING A MESSAGE
        # admin_user = await check_feature_restriction(message)

        # community_settings = await get_community_settings(message)

        # if admin_user is True and community_settings['enabled_twitter_request'] is True:
        #     received_dm = await receive_twitter_username_through_dm(message)
        # if received_dm is True:
        #     return True

        #     twitter_handle = await get_twitter_handle(message)
        # else:
        #     twitter_handle = ''
        # # Receive and handle follow-up

        # discord_message = record_message(message, twitter_handle)
        # discord_message.data[0]['message_id'] = discord_message.data[0]['id']

        # top_message = await check_top_message_eligibility_old(discord_message.data)

        # if top_message is not False:
        #     inst = AirTableServiceSupabase()
        #     inst.update_or_create_a_single_record(config.AIRTABLE_TABLE_NAME_TOP_MESSAGES, top_message.data[0])

        # for action, args, kwargs in message_actions:
        #     args = tuple(args) if isinstance(args, (list, tuple)) else (args,)
        #     kwargs = dict(()) if kwargs is None else dict(kwargs)
        #     print(f'{action}(*{args}, **{kwargs})')
        #     if action and action.lower().startswith('insert'):
        #         # TODO: make this async
        #         log(f'Found {action}')
        #         # SUPA.table('discord_message').insert(message_dict).execute()
        #     else:
        #         await get_coroutine(message, action)(*args, **kwargs)


async def get_reaction_threshold_value():
    """ Retrieves the threshold value for when a DiscordMessage entry is added to the TopMessage table """
    try:
        reaction_rule = SUPA.table('stigapp_rule').select("*").eq('rule_name', 'reactions rule').execute()
        reaction_threshold = reaction_rule.data[0].get('threshold')
    except:
        reaction_threshold = 5
    return reaction_threshold


async def check_top_message_eligibility(message_data):
    """ Checks whether a message in the DiscordMessage table has passed the unique user reaction threshold to be included in TopMessage

    Uses 'reactions rule' in the rule database table if available.
    If not, the treshold is the number of reactions from 5 unique users
    """

    reaction_threshold = await get_reaction_threshold_value()

    # NOTE: Extend and combine rules here
    if message_data.get('unique_user_count', -1) >= reaction_threshold:
        return True
    else:
        return False


async def build_general_message_object(payload, trigger_event, community_settings=None):
    """ Uses a payload to build all the message fields used throughout the DiscordMessage, TopMessage, and GardenMessage tables

    Inputs
    - payload: The Discord payload object from on_raw_reaction_add/remove
    - trigger_event: 'add' of 'remove'
    """
    if trigger_event == 'message':
        message = payload
    else:
        message = await client.get_channel(payload.channel_id).fetch_message(payload.message_id)
    all_reactions = await build_reaction_history(message)
    forum_tags = []
    message_type, message_channel, message_thread_title = get_message_type_and_channel(message)

    if isinstance(message.channel, Thread):
        print("Thread Type")
        print(message.channel.type)
        print("parent")
        print(message.channel.parent)
        print("Tags!!!!!!!")
        print(message.channel.applied_tags)

    print("============================")
    print("============================")

    extracted_urls = extract_urls_from_message(message)

    twitter_handle = await get_twitter_handle(message, 'reaction', community_settings)

    site_id = await get_site_id(message.guild.name)

    message_data = dict(
        # Discord Message Fields
        id=message.id,
        author=to_dict(message.author, AUTHOR_ATTRS),
        author_discord_username=f"{message.author.name}#{message.author.discriminator}",
        authorship_datetime=isoformat(message.created_at),
        channel_name=message_channel,
        message_thread_title=message_thread_title,
        content=str(message.content),
        created_at=isoformat(message.created_at),
        forum_tags=forum_tags,
        guild_name=message.guild.name,
        extracted_urls_id=extracted_urls,
        jump_url=str(message.jump_url),
        message_type=message_type,
        reactions=all_reactions,
        saved_at=isoformat(datetime.datetime.now()),

        # TopMessage/GardenMessage Fields
        original_message=str(message.content),
        editable_message=str(message.content),
        added_to_tarden=False,
        message_id=message.id,
        twitter_handle=twitter_handle,

        # Djagno Field
        django_site_id=site_id,
    )
    reaction_dict = make_reaction_data_object(all_reactions)

    message_data.update(reaction_dict)

    return message_data


def select_default_or_existing_field_values(general_message_data, existing_data, field):
    """ Checks whether a field has an existing value or not.  If it does, the existing value is preserved """

    return existing_data.get(field) if existing_data.get(field) != '' and existing_data.get(field) != None else general_message_data.get(field)


async def build_message_object_by_table(general_message_data, table_name):
    """ Uses the general message object that contains all fields used across all tables and filters the specific fields needed for a specific table

    Inputs
    - general_message_data: all fields constructed from the current data
    - table_name: the table the message data should be updated in
    """
    query_result = SUPA.table(table_name).select("*").eq('id', general_message_data['message_id']).execute()
    existing_data = query_result.data[0] if query_result.data != [] else {}

    prepared_message_data = {}
    if table_name == 'discord_message':
        for field_name in discord_message_table_fields.split(' '):
            if general_message_data.get(field_name):
                prepared_message_data[field_name] = general_message_data[field_name]

    elif table_name == 'top_message' or table_name == 'garden_message':
        if table_name == 'top_message':
            for field_name in top_message_table_fields.split(' '):
                if general_message_data.get(field_name):
                    prepared_message_data[field_name] = general_message_data[field_name]
            prepared_message_data['added_to_garden'] = select_default_or_existing_field_values(
                general_message_data, existing_data, 'added_to_garden')
        elif table_name == 'garden_message':
            for field_name in garden_message_table_fields.split(' '):
                if general_message_data.get(field_name):
                    prepared_message_data[field_name] = general_message_data[field_name]
        # For these fields, the existing data needs to be preserved
        editable_message = existing_data.get('editable_message') if existing_data.get(
            'editable_message') != '' and existing_data.get('editable_message') != None else prepared_message_data['original_message']
        prepared_message_data['editable_message'] = editable_message

        prepared_message_data['exported_at'] = select_default_or_existing_field_values(
            general_message_data, existing_data, 'exported_at')
        prepared_message_data['edited_at'] = select_default_or_existing_field_values(
            general_message_data, existing_data, 'edited_at')
        prepared_message_data['twitter_handle'] = select_default_or_existing_field_values(
            general_message_data, existing_data, 'twitter_handle')
    else:
        # Log error
        pass

    return prepared_message_data


def update_airtable(table_name, message_data, update_action_type):
    """
    Calls the appropriate Airtable class function from airtable_integration_supabase.py

    Inputs
    table_name: TopMessage or GardenMessage env value
    message_data: json object
    update_action_type: 'delete' or 'update'
    """

    # NOTE: delete is based on a payload, not json
    inst = AirTableServiceSupabase()

    if update_action_type == 'update':
        inst.update_or_create_a_single_record(table_name, message_data)
    elif update_action_type == 'delete':
        inst.delete_airtable_entry(table_name, message_data.id)
    else:
        # log error
        pass


async def trigger_reward_reactions():
    results = SUPA.table('reward_reaction_log').select("*").eq('reaction_sent', False).execute()

    if len(results.data) > 0:
        for message in results.data:
            await add_reaction_to_top_message(message['message_id'], message['message_type'])


async def handle_top_and_garden_table_updates(general_message, operation):

    # Check threshold
    eligible_top_message = await check_top_message_eligibility(general_message)
    tm_entry = SUPA.table('top_message').select("*").eq('message_id', general_message['message_id']).execute()
    # If it meets the threshold
    if eligible_top_message is True:
        # Build a TopMessage dict
        top_message_data = await build_message_object_by_table(general_message, 'top_message')

        # Update or create TopMessage table entry
        if tm_entry.data == []:
            query = SUPA.table('top_message').insert(top_message_data).execute()
        else:
            SUPA.table('top_message').update(top_message_data).eq("message_id", top_message_data['message_id']).execute()

        # Update Airtable
        update_airtable(config.AIRTABLE_TABLE_NAME_TOP_MESSAGES, top_message_data, 'update')

        if operation == 'Add':
            await trigger_reward_reactions()
    elif eligible_top_message is False and operation == 'Remove':
        # If TM is in the database and it doesn't meet the threshold
        if tm_entry != []:
            # Remove it from the database
            SUPA.table('top_message').delete().eq("message_id", payload.message_id).execute()

            # Remove it from the Airtable
            update_airtable(config.AIRTABLE_TABLE_NAME_TOP_MESSAGES, payload.message_id, 'delete')

    # If a GardenMessage entry exists
    garden_message_query_result = SUPA.table('garden_message').select(
        "*").eq('message_id', top_message_data['message_id']).execute()

    if garden_message_query_result.data != []:
        # Build a GardenMessage dict
        garden_message_data = await build_message_object_by_table(general_message, 'garden_message')

        # Update the GardenMessage table
        updated_garden_message = SUPA.table('garden_message').update(
            garden_message_data).eq("message_id", garden_message_data['message_id']).execute()

        # Update Airtable
        update_airtable(config.AIRTABLE_TABLE_NAME_GARDEN, garden_message_data, 'update')


@ client.event
async def on_raw_reaction_add(payload):
    """ Updates the message data across all tables when a reaction is added to a message

    on_raw_reaction_add will pick up any interaction adding a reaction, regardless of whether the message is in the cache or not (unlike on_reaction_add)
    """
    message = await client.get_channel(payload.channel_id).fetch_message(payload.message_id)
    general_message = await build_general_message_object(payload, 'add')
    discord_message_data = await build_message_object_by_table(general_message, 'discord_message')

    if payload.emoji.is_custom_emoji():
        reaction = discord.utils.get(message.reactions, emoji=payload.emoji)
    else:
        reaction = discord.utils.get(message.reactions, emoji=payload.emoji.name)

    user = payload.member

    # TODO: Simplify this.  Just use DiscordMessage update_or_create like on_raw_reaction_remove
    actions = DEFAULT_REACTION_ACTIONS
    assert actions[0][0] == 'record_reaction'

    if reaction.message.author == client.user:
        log(f'message author ({reaction.message.author}) == client.user ({client.user})')

    responses = []
    # TODO: This probably expects a Message object through Pycord, so it won't work with general_message
    if reaction.emoji:
        assert actions[0][0] == 'record_reaction'
        for fun_name, args, kwargs in actions:
            fun_name = str(fun_name)
            assert fun_name == 'record_reaction'  # only one action allowed, for now
            fun, args, kwargs = get_fun_args_kwargs(fun_name, *args, **kwargs)
            kwargs.update(dict(
                message=message,
                reaction=reaction,
                user=user))
            log(f'for fun in actions...\nfun: {fun}\nkwargs: {kwargs}')
            resp = fun(*args, **kwargs)
            log(f'fun response: {resp}')
            responses.append(resp)
    #??????????????????????????????????????

    # If DiscordMessage exists, update it
    discord_message_query_result = SUPA.table('discord_message').select("*").eq('id', discord_message_data['id']).execute()

    if discord_message_query_result.data == []:
        SUPA.table('discord_message').insert(discord_message_data).execute()
    # Else, create it
    else:
        SUPA.table('discord_message').update(discord_message_data).eq('id', discord_message_data['id']).execute()

    await handle_top_and_garden_table_updates(general_message, 'Add')


@ client.event
async def on_raw_reaction_remove(payload):
    """ Updates the message data across all tables when a reaction is removed from a message

    on_raw_reaction_remove will pick up any interaction removing a reaction, regardless of whether the message is in the cache or not (unlike on_reaction_remove)
    """
    message = await client.get_channel(payload.channel_id).fetch_message(payload.message_id)
    general_message = await build_general_message_object(payload, 'remove')

    # TODO: Could make "build_message_object" return a tuple ({}, True/False) - True/False could be if it already exists or not
    discord_message_data = await build_message_object_by_table(general_message, 'discord_message')

    existing_data = SUPA.table('discord_message').select("*").eq('id', general_message['message_id']).execute()

    # Handle DiscordMessage table
    # If there is an existing DiscordMessage entry
    if existing_data.data != []:
        discord_message_entry = update_or_create(table_name='discord_message', record_dict=discord_message_data)
    else:
        existing_discord_message = SUPA.table('discord_message').update(
            discord_message_data).eq("id", discord_message_data['message_id']).execute()

    # Handle TopMessage table
    """
    Possibilities
    1. TM exists & above treshold still
    2. TM exists and newly above= threshold
    3. TM exists and went below threshold
    4. TM doesn't exist & above= threshold
    5. TM doesn't exist and below threshold
    """

    await handle_top_and_garden_table_updates(general_message, 'Remove')

if __name__ == '__main__':
    client.run(config.DISCORD_BOT_TOKEN)
